/* 
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE. 
*/

#ifndef glmath_glmvec4_h
#define glmath_glmvec4_h

#include "common.h"


/**
* Creates a new, empty GLMVec4
*
* @returns Reference to a new 3D vector
*/
GLMVec4Ref GLMVec4Create()
{
    GLMVec4Ref out = (GLMVec4Ref)GLM_ALLOCATE(GLMVec4);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(0.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);

    return out;
}


/**
* Creates a new GLMVec4 initialized with values from an existing vector
*
* @param Reference to a vector to clone
* @returns Reference to a new 3D vector
*/
GLMVec4Ref GLMVec4Clone(GLMVec4Ref a)
{
    GLMVec4Ref out = GLMVec4Create();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];

    return out;
}


/**
* Creates a new GLMVec4 initialized with the given values
*
* @param x X component
* @param y Y component
* @param z Z component
* @param w W component
* @returns a reference to new 4D vector
*/
GLMVec4Ref GLMVec4FromValues(GLMRational x, GLMRational y, GLMRational z, GLMRational w)
{
    GLMVec4Ref out = GLMVec4Create();

    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
    return out;
}

/**
* Copy the values from one GLMVec4 to another
*
* @param out reference to the receiving vector
* @param a reference to the source vector
*/
void GLMVec4Copy(__OUT__ GLMVec4Ref out, GLMVec4Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
}

/**
* Set the components of a GLMVec4 to the given values
*
* @param out reference to the receiving vector
* @param x X component
* @param y Y component
* @param z Z component
* @param w W component
*/
void GLMVec4Set(__OUT__ GLMVec4Ref out, GLMRational x, GLMRational y, GLMRational z, GLMRational w)
{
    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = w;
}

/**
* Adds two GLMVec4's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec4Add(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
}

/**
* Subtracts vector b from vector a
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec4Subtract(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
}

/**
* Multiplies two GLMVec4's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec4Multiply(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
    out[3] = a[3] * b[3];
}

/**
* Divides two GLMVec4's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec4Divide(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
    out[3] = a[3] / b[3];
}

/**
* Returns the minimum of two GLMVec4's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec4Min(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = fminf(a[0], b[0]);
    out[1] = fminf(a[1], b[1]);
    out[2] = fminf(a[2], b[2]);
    out[3] = fminf(a[3], b[3]);
}

/**
* Returns the maximum of two GLMVec4's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec4Max(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b)
{
    out[0] = GLM_MAX(a[0], b[0]);
    out[1] = GLM_MAX(a[1], b[1]);
    out[2] = GLM_MAX(a[2], b[2]);
    out[3] = GLM_MAX(a[3], b[3]);
}

/**
* Scales a GLMVec4 by a scalar number
*
* @param out reference to the receiving vector
* @param a reference to the vector to scale
* @param b amount to scale the vector by
*/
void GLMVec4Scale(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
}

/**
* Adds two GLMVec4's after scaling the second operand by a scalar value
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b by before adding
*/
void GLMVec4ScaleAndAdd(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
}


/**
* Calculates the euclidian distance between two GLMVec4's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns distance between a and b
*/
GLMRational GLMVec4Distance(GLMVec4Ref a, GLMVec4Ref b)
{
    GLMRational x = b[0] - a[0],
                y = b[1] - a[1],
                z = b[2] - a[2],
                w = b[3] - a[3];

    return GLM_SQRT(x*x + y*y + z*z + w*w);
}


/**
* Calculates the squared euclidian distance between two GLMVec4's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns squared distance between a and b
*/
GLMRational GLMVec4SquaredDistance(GLMVec4Ref a, GLMVec4Ref b)
{
    GLMRational x = b[0] - a[0],
                y = b[1] - a[1],
                z = b[2] - a[2],
                w = b[3] - a[3];

    return x*x + y*y + z*z + w*w;
}


/**
* Calculates the length of a GLMVec4
*
* @param a reference to a vector to calculate length of
* @returns length of a
*/
GLMRational GLMVec4Length(GLMVec4Ref a)
{
    GLMRational x = a[0],
                y = a[1],
                z = a[2],
                w = a[3];

    return GLM_SQRT(x*x + y*y + z*z + w*w);
}


/**
* Calculates the squared length of a GLMVec4
*
* @param a reference to vector to calculate squared length of
* @returns squared length of a
*/
GLMRational GLMVec4SquaredLength(GLMVec4Ref a)
{
    GLMRational x = a[0],
                y = a[1],
                z = a[2],
                w = a[3];

    return x*x + y*y + z*z + w*w;
}

/**
* Negates the components of a GLMVec4
*
* @param out reference to the receiving vector
* @param a reference to vector to negate
*/
void GLMVec4Negate(__OUT__ GLMVec4Ref out, GLMVec4Ref a)
{
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] = -a[3];
}

/**
* Returns the inverse of the components of a GLMVec4
*
* @param out reference to the receiving vector
* @param a reference to vector to invert
*/
void GLMVec4Inverse(__OUT__ GLMVec4Ref out, GLMVec4Ref a)
{
    out[0] = GLM_RATIONAL(1.0) / a[0];
    out[1] = GLM_RATIONAL(1.0) / a[1];
    out[2] = GLM_RATIONAL(1.0) / a[2];
    out[3] = GLM_RATIONAL(1.0) / a[3];
}

/**
* Normalize a GLMVec4
*
* @param out reference to the receiving vector
* @param a reference to vector to normalize
*/
void GLMVec4Normalize(__OUT__ GLMVec4Ref out, GLMVec4Ref a)
{
    GLMRational x = a[0],
                y = a[1],
                z = a[2],
                w = a[3];

    GLMRational len = x*x + y*y + z*z + w*w;

    if (len > 0) 
    {
        len = GLM_RATIONAL(1.0) / GLM_SQRT(len);
        out[0] = x * len;
        out[1] = y * len;
        out[2] = z * len;
        out[3] = w * len;
    }
}

/**
* Calculates the dot product of two GLMVec4's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns dot product(scalar) of a and b
*/
GLMRational GLMVec4Dot(GLMVec4Ref a, GLMVec4Ref b)
{
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
}

/**
* Performs a linear interpolation between two GLMVec4's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param t interpolation amount between the two inputs
*/
void GLMVec4Lerp(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMVec4Ref b, GLMRational t)
{
    GLMRational ax = a[0],
                ay = a[1],
                az = a[2],
                aw = a[3];

    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
    out[3] = aw + t * (b[3] - aw);
}


/**
* Generates a random GLMVec4 vector with the given scale
*
* @param out the receiving vector
* @param scale Length of the resulting vector. 
*/
void GLMVec4Random(__OUT__ GLMVec4Ref out, GLMRational scale)
{
    out[0] = GLMRandom();
    out[1] = GLMRandom();
    out[2] = GLMRandom();
    out[3] = GLMRandom();

    GLMVec4Normalize(out, out);
    GLMVec4Scale(out, out, scale);
}

/**
* Transforms the GLMVec4 with a GLMMat4.
*
* @param out reference to the receiving vector
* @param a reference to the vector to transform
* @param m reference to matrix to transform with
*/
void GLMVec4TransformMat4(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMMat4Ref m)
{
    GLMRational x = a[0], y = a[1], z = a[2], w = a[3];

    out[0] = m[0] * x + m[4] * y + m[8] * z + m[12] * w;
    out[1] = m[1] * x + m[5] * y + m[9] * z + m[13] * w;
    out[2] = m[2] * x + m[6] * y + m[10] * z + m[14] * w;
    out[3] = m[3] * x + m[7] * y + m[11] * z + m[15] * w;
}

/**
* Transforms the GLMVec4 with a GLMQuat
*
* @param out reference to the receiving vector
* @param a reference to the vector to transform
* @param q reference to quaternion to transform with
*/
void GLMVec4TransformQuat(__OUT__ GLMVec4Ref out, GLMVec4Ref a, GLMQuatRef q)
{
    GLMRational x = a[0], y = a[1], z = a[2],
                qx = q[0], qy = q[1], qz = q[2], qw = q[3],

    // calculate GLMQuat * GLMVec4
    ix =  qw * x + qy * z - qz * y,
    iy =  qw * y + qz * x - qx * z,
    iz =  qw * z + qx * y - qy * x,
    iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse GLMQuat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;
    out[3] = a[3];
}

/**
* Writes string representations for GLMVec4 in out parameter.
* out must be at least 64 bytes long.
*
* @param out recieving string buffer
* @param a reference to vector to represent as a string
*/
void GLMVec4Str(__OUT__ char *const out, GLMVec4Ref a)
{
    sprintf_s(out, 64, "GLMVec4(%f, %f, %f, %f)", a[0], a[1], a[2], a[3]);
}


/**
* Returns whether or not the vectors have exactly the same values.
*
* @param a reference to the first vector.
* @param b reference to the second vector.
* @returns True if the vectors are equal, false otherwise.
*/
bool GLMVec4ExactEquals(GLMVec4Ref a, GLMVec4Ref b)
{
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
}

/**
* Returns whether or not the vectors have approximately the same elements in the same position.
*
* @param a reference to the first vector.
* @param b reference to the second vector.
* @returns true if the vectors are equal, false otherwise.
*/
bool GLMVec4Equals(GLMVec4Ref a, GLMVec4Ref b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];

    return (GLM_ABS(a0 - b0) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a0), GLM_ABS(b0)) ) &&
            GLM_ABS(a1 - b1) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a1), GLM_ABS(b1)) ) &&
            GLM_ABS(a2 - b2) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a2), GLM_ABS(b2)) ) &&
            GLM_ABS(a3 - b3) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a3), GLM_ABS(b3)) ));
}

#endif