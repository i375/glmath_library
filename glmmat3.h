/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmat3_h
#define glmath_glmat3_h

#include "common.h"


/**
* Creates a new identity GLMMat3
*
* @returns reference to a new 3x3 matrix
*/
GLMMat3Ref GLMMat3Create()
{
    GLMMat3Ref out = (GLMMat3Ref)GLM_ALLOCATE(GLMMat3);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(0.0);
    out[4] = GLM_RATIONAL(1.0);
    out[5] = GLM_RATIONAL(0.0);
    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(1.0);

    return out;
}

/**
* Copies the upper-left 3x3 values from GLMMat4 into the given GLMMat3.
*
* @param out reference to the receiving 3x3 matrix
* @param a reference tothe source 4x4 matrix
*/
void GLMMat3SetFromMat4(__OUT__ GLMMat3Ref out, GLMMat4Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[4];
    out[4] = a[5];
    out[5] = a[6];
    out[6] = a[8];
    out[7] = a[9];
    out[8] = a[10];
}


/**
* Creates a new GLMMat3 initialized with values from an existing matrix
*
* @param {mat3} a matrix to clone
* @returns {mat3} a new 3x3 matrix
*/
GLMMat3Ref GLMMat3Clone(GLMMat3Ref a)
{
    GLMMat3Ref out = GLMMat3Create();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];

    return out;
}

/**
* Copy the values from one GLMMat3 to another
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat3Copy(__OUT__ GLMMat3Ref out, GLMMat3Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
}


/**
* Create a new GLMMat3 with the given values
*
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m02 Component in column 0, row 2 position (index 2)
* @param m10 Component in column 1, row 0 position (index 3)
* @param m11 Component in column 1, row 1 position (index 4)
* @param m12 Component in column 1, row 2 position (index 5)
* @param m20 Component in column 2, row 0 position (index 6)
* @param m21 Component in column 2, row 1 position (index 7)
* @param m22 Component in column 2, row 2 position (index 8)
* @returns reference to a new GLMMat3
*/
GLMMat3Ref GLMMat3FromValues
   (GLMRational m00, GLMRational m01, GLMRational m02,
    GLMRational m10, GLMRational m11, GLMRational m12,
    GLMRational m20, GLMRational m21, GLMRational m22)
{
    GLMMat3Ref out = GLMMat3Create();

    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m10;
    out[4] = m11;
    out[5] = m12;
    out[6] = m20;
    out[7] = m21;
    out[8] = m22;

    return out;
}

/**
* Set the components of a GLMMat3 to the given values
*
* @param out reference to the receiving matrix
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m02 Component in column 0, row 2 position (index 2)
* @param m10 Component in column 1, row 0 position (index 3)
* @param m11 Component in column 1, row 1 position (index 4)
* @param m12 Component in column 1, row 2 position (index 5)
* @param m20 Component in column 2, row 0 position (index 6)
* @param m21 Component in column 2, row 1 position (index 7)
* @param m22 Component in column 2, row 2 position (index 8)
*/
void GLMMat3Set(__OUT__ GLMMat3Ref out,
    GLMRational m00, GLMRational m01, GLMRational m02,
    GLMRational m10, GLMRational m11, GLMRational m12,
    GLMRational m20, GLMRational m21, GLMRational m22)
{
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m10;
    out[4] = m11;
    out[5] = m12;
    out[6] = m20;
    out[7] = m21;
    out[8] = m22;
}

/**
* Set a GLMMat3 to the identity matrix
*
* @param out reference to the receiving matrix
*/
void GLMMat3Identity(__OUT__ GLMMat3Ref out)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(0.0);
    out[4] = GLM_RATIONAL(1.0);
    out[5] = GLM_RATIONAL(0.0);
    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(1.0);
}

/**
* Transpose the values of a GLMMat3
*
* @param out reference to the receiving matrix
* @param a reference the source matrix
*/
void GLMMat3Transpose(__OUT__ GLMMat3Ref out, GLMMat3Ref a)
{

    if (out == a) 
    {
        GLMRational a01 = a[1], a02 = a[2], a12 = a[5];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a01;
        out[5] = a[7];
        out[6] = a02;
        out[7] = a12;
    }
    else 
    {
        out[0] = a[0];
        out[1] = a[3];
        out[2] = a[6];
        out[3] = a[1];
        out[4] = a[4];
        out[5] = a[7];
        out[6] = a[2];
        out[7] = a[5];
        out[8] = a[8];
    }

}

/**
* Inverts a GLMMat3
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat3Invert(__OUT__ __NULLABLE__ GLMMat3Ref out, GLMMat3Ref a)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
                a10 = a[3], a11 = a[4], a12 = a[5],
                a20 = a[6], a21 = a[7], a22 = a[8],

    b01 =  a22 * a11 - a12 * a21,
    b11 = -a22 * a10 + a12 * a20,
    b21 =  a21 * a10 - a11 * a20,

    // Computing determinant
    det = a00 * b01 + a01 * b11 + a02 * b21;

    if (det == GLM_RATIONAL(0.0))
    {
        out = NULL;
        return;
    }

    det = GLM_RATIONAL(1.0) / det;

    out[0] =   b01 * det;
    out[1] = (-a22 * a01 + a02 * a21) * det;
    out[2] = ( a12 * a01 - a02 * a11) * det;
    out[3] =   b11 * det;
    out[4] = ( a22 * a00 - a02 * a20) * det;
    out[5] = (-a12 * a00 + a02 * a10) * det;
    out[6] =   b21 * det;
    out[7] = (-a21 * a00 + a01 * a20) * det;
    out[8] = ( a11 * a00 - a01 * a10) * det;
}

/**
* Calculates the adjugate of a GLMMat3
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat3Adjoint(__OUT__ GLMMat3Ref out, GLMMat3Ref a)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
               a10 = a[3], a11 = a[4], a12 = a[5],
               a20 = a[6], a21 = a[7], a22 = a[8];

    out[0] = (a11 * a22 - a12 * a21);
    out[1] = (a02 * a21 - a01 * a22);
    out[2] = (a01 * a12 - a02 * a11);
    out[3] = (a12 * a20 - a10 * a22);
    out[4] = (a00 * a22 - a02 * a20);
    out[5] = (a02 * a10 - a00 * a12);
    out[6] = (a10 * a21 - a11 * a20);
    out[7] = (a01 * a20 - a00 * a21);
    out[8] = (a00 * a11 - a01 * a10);
}


/**
* Computes the determinant of a GLMMat3
*
* @param a reference to the source matrix
*/
GLMRational GLMMat3Determinant(GLMMat3Ref a)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
               a10 = a[3], a11 = a[4], a12 = a[5],
               a20 = a[6], a21 = a[7], a22 = a[8];

    return a00 * (a22 * a11 - a12 * a21) + a01 * (-a22 * a10 + a12 * a20) + a02 * (a21 * a10 - a11 * a20);
}

/**
* Multiplies two GLMMat3's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat3Multiply(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMMat3Ref b)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
               a10 = a[3], a11 = a[4], a12 = a[5],
               a20 = a[6], a21 = a[7], a22 = a[8],

               b00 = b[0], b01 = b[1], b02 = b[2],
               b10 = b[3], b11 = b[4], b12 = b[5],
               b20 = b[6], b21 = b[7], b22 = b[8];

    out[0] = b00 * a00 + b01 * a10 + b02 * a20;
    out[1] = b00 * a01 + b01 * a11 + b02 * a21;
    out[2] = b00 * a02 + b01 * a12 + b02 * a22;

    out[3] = b10 * a00 + b11 * a10 + b12 * a20;
    out[4] = b10 * a01 + b11 * a11 + b12 * a21;
    out[5] = b10 * a02 + b11 * a12 + b12 * a22;

    out[6] = b20 * a00 + b21 * a10 + b22 * a20;
    out[7] = b20 * a01 + b21 * a11 + b22 * a21;
    out[8] = b20 * a02 + b21 * a12 + b22 * a22;
}

/**
* Translate a GLMMat3 by the given vector
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to translate
* @param v reference to vector to translate by
*/
void GLMMat3Translate(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMVec2Ref v)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
               a10 = a[3], a11 = a[4], a12 = a[5],
               a20 = a[6], a21 = a[7], a22 = a[8],
               x = v[0], y = v[1];

    out[0] = a00;
    out[1] = a01;
    out[2] = a02;

    out[3] = a10;
    out[4] = a11;
    out[5] = a12;

    out[6] = x * a00 + y * a10 + a20;
    out[7] = x * a01 + y * a11 + a21;
    out[8] = x * a02 + y * a12 + a22;
}

/**
* Rotates a GLMMat3 by the given angle
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat3Rotate(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMRational rad)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2],
               a10 = a[3], a11 = a[4], a12 = a[5],
               a20 = a[6], a21 = a[7], a22 = a[8],

               s = sinf(rad),
               c = cosf(rad);

    out[0] = c * a00 + s * a10;
    out[1] = c * a01 + s * a11;
    out[2] = c * a02 + s * a12;

    out[3] = c * a10 - s * a00;
    out[4] = c * a11 - s * a01;
    out[5] = c * a12 - s * a02;

    out[6] = a20;
    out[7] = a21;
    out[8] = a22;
}

/**
* Scales the GLMMat3 by the dimensions in the given GLMVec2
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param v reference to the GLMVec2 to scale the matrix by
**/
void GLMMat3Scale(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMVec2Ref v)
{
    GLMRational x = v[0], y = v[1];

    out[0] = x * a[0];
    out[1] = x * a[1];
    out[2] = x * a[2];

    out[3] = y * a[3];
    out[4] = y * a[4];
    out[5] = y * a[5];

    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
}

/**
* Sets a matrix from a vector translation
*
* @param out reference to source and receiving operation result
* @param v reference to translation vector
*/
void GLMMat3FromTranslation(__IN_OUT__ GLMMat3Ref out, GLMVec2Ref v)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(0.0);
    out[4] = GLM_RATIONAL(1.0);
    out[5] = GLM_RATIONAL(0.0);
    out[6] = v[0];
    out[7] = v[1];
    out[8] = GLM_RATIONAL(1.0);
}


/**
* Sets a matrix from a given angle
*
* @param out reference to source and receiving operation result
* @param rad the angle to rotate the matrix by
*/
void GLMMat3FromRotation(__IN_OUT__ GLMMat3Ref out, GLMRational rad)
{
    GLMRational s = sinf(rad), c = cosf(rad);

    out[0] = c;
    out[1] = s;
    out[2] = GLM_RATIONAL(0.0);

    out[3] = -s;
    out[4] = c;
    out[5] = GLM_RATIONAL(0.0);

    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(1.0);
}


/**
* Sets a matrix from a vector scaling
*
* @param out reference to source and receiving operation result
* @param v reference to scaling vector
*/
void GLMMat3FromScaling(__IN_OUT__ GLMMat3Ref out, GLMVec2Ref v)
{
    out[0] = v[0];
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);

    out[3] = GLM_RATIONAL(0.0);
    out[4] = v[1];
    out[5] = GLM_RATIONAL(0.0);

    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(1.0);
}


/**
* Copies the values from a GLMMat2d into a GLMMat3
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to copy
**/
void GLMMat3FromMat2d(__IN_OUT__ GLMMat3Ref out, GLMMat2dRef a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = GLM_RATIONAL(0.0);

    out[3] = a[2];
    out[4] = a[3];
    out[5] = GLM_RATIONAL(0.0);

    out[6] = a[4];
    out[7] = a[5];
    out[8] = GLM_RATIONAL(1.0);
}


/**
* Computes a 3x3 matrix from the given quaternion
*
* @param out reference to receiving operation result
* @param q reference quaternion to create matrix from
*/
void GLMMat3FromQuat(__IN_OUT__ GLMMat3Ref out, GLMQuatRef q)
{
    GLMRational x = q[0], y = q[1], z = q[2], w = q[3],
                x2 = x + x,
                y2 = y + y,
                z2 = z + z,

                xx = x * x2,
                yx = y * x2,
                yy = y * y2,
                zx = z * x2,
                zy = z * y2,
                zz = z * z2,
                wx = w * x2,
                wy = w * y2,
                wz = w * z2;

    out[0] = GLM_RATIONAL(1.0) - yy - zz;
    out[3] = yx - wz;
    out[6] = zx + wy;

    out[1] = yx + wz;
    out[4] = GLM_RATIONAL(1.0) - xx - zz;
    out[7] = zy - wx;

    out[2] = zx - wy;
    out[5] = zy + wx;
    out[8] = GLM_RATIONAL(1.0) - xx - yy;
}

/**
* Computes a 3x3 normal matrix (transpose inverse) from the 4x4 matrix
*
* @param out reference to receiving operation result
* @param a reference to GLMMat4 to derive the normal matrix from
*/
void GLMMat3NormalFromMat4(__OUT__ __NULLABLE__ GLMMat3Ref out, GLMMat4Ref a)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
                a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
                a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
                a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

                b00 = a00 * a11 - a01 * a10,
                b01 = a00 * a12 - a02 * a10,
                b02 = a00 * a13 - a03 * a10,
                b03 = a01 * a12 - a02 * a11,
                b04 = a01 * a13 - a03 * a11,
                b05 = a02 * a13 - a03 * a12,
                b06 = a20 * a31 - a21 * a30,
                b07 = a20 * a32 - a22 * a30,
                b08 = a20 * a33 - a23 * a30,
                b09 = a21 * a32 - a22 * a31,
                b10 = a21 * a33 - a23 * a31,
                b11 = a22 * a33 - a23 * a32,

                // Computing the determinant
                det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (det == GLM_RATIONAL(0.0))
    {
        out = NULL;
        return;
    }

    det = GLM_RATIONAL(1.0) / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[2] = (a10 * b10 - a11 * b08 + a13 * b06) * det;

    out[3] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[4] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[5] = (a01 * b08 - a00 * b10 - a03 * b06) * det;

    out[6] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[7] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[8] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
}

/**
* Writes string representations for GLMMat3 in out parameter.
* out must be at least 256 bytes long.
*
* @param out recieving string buffer
* @param a reference to matrix to represent as a string
*/
void GLMMat3Str(__OUT__ char *const out, GLMMat3Ref a)
{
    sprintf_s(out, 256, "GLMMat2(%f, %f, %f, %f, %f, %f, %f, %f, %f)", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8]);
}

/**
* Computes Frobenius norm of a GLMMat3
*
* @param a reference to the matrix to calculate Frobenius norm of
* @returns Frobenius norm
*/
GLMRational GLMMat3Frob(GLMMat3Ref a)
{
    return GLM_SQRT(GLM_POW(a[0], GLM_RATIONAL(2.0)) + GLM_POW(a[1], GLM_RATIONAL(2.0)) + GLM_POW(a[2], GLM_RATIONAL(2.0)) + GLM_POW(a[3], GLM_RATIONAL(2.0)) + GLM_POW(a[4], GLM_RATIONAL(2.0)) + GLM_POW(a[5], GLM_RATIONAL(2.0)) + GLM_POW(a[6], GLM_RATIONAL(2.0)) + GLM_POW(a[7], GLM_RATIONAL(2.0)) + GLM_POW(a[8], GLM_RATIONAL(2.0)));
}

/**
* Adds two GLMMat3's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat3Add(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMMat3Ref b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
    out[6] = a[6] + b[6];
    out[7] = a[7] + b[7];
    out[8] = a[8] + b[8];
}

/**
* Subtracts matrix b from matrix a
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat3Subtract(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMMat3Ref b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
    out[6] = a[6] - b[6];
    out[7] = a[7] - b[7];
    out[8] = a[8] - b[8];
}

/**
* Multiply each element of the matrix by a scalar.
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to scale
* @param b amount to scale the matrix's elements by
*/
void GLMMat3MultiplyScalar(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
    out[6] = a[6] * b;
    out[7] = a[7] * b;
    out[8] = a[8] * b;
}

/**
* Adds two GLMat3's after multiplying each element of the second operand by a scalar value.
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b's elements by before adding
*/
void GLMMat3MultiplyScalarAndAdd(__OUT__ GLMMat3Ref out, GLMMat3Ref a, GLMMat3Ref b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    out[4] = a[4] + (b[4] * scale);
    out[5] = a[5] + (b[5] * scale);
    out[6] = a[6] + (b[6] * scale);
    out[7] = a[7] + (b[7] * scale);
    out[8] = a[8] + (b[8] * scale);
}

/*
* Returns whether or not the matrices have exactly the same element values
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat3ExactEquals(GLMMat3Ref a, GLMMat3Ref b)
{
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] &&
            a[3] == b[3] && a[4] == b[4] && a[5] == b[5] &&
            a[6] == b[6] && a[7] == b[7] && a[8] == b[8];
}

bool GLMMat3Equals(GLMMat3Ref a, GLMMat3Ref b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5], a6 = a[6], a7 = a[7], a8 = a[8];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5], b6 = a[6], b7 = b[7], b8 = b[8];

    return (GLM_ABS(a0 - b0) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a0), GLM_ABS(b0)) ) &&
            GLM_ABS(a1 - b1) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a1), GLM_ABS(b1)) ) &&
            GLM_ABS(a2 - b2) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a2), GLM_ABS(b2)) ) &&
            GLM_ABS(a3 - b3) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a3), GLM_ABS(b3)) ) &&
            GLM_ABS(a4 - b4) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a4), GLM_ABS(b4)) ) &&
            GLM_ABS(a5 - b5) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a5), GLM_ABS(b5)) ) &&
            GLM_ABS(a6 - b6) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a6), GLM_ABS(b6)) ) &&
            GLM_ABS(a7 - b7) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a7), GLM_ABS(b7)) ) &&
            GLM_ABS(a8 - b8) <= GLM_EPSILON*GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a8), GLM_ABS(b8)) ));
}

#endif