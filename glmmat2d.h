/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmat2d_h
#define glmath_glmat2d_h

#include "common.h"

/**
* Creates a new identity GLMMat2d
*
* @returns a new 2x3 matrix
*/
GLMMat2dRef GLMMat2dCreate()
{
    GLMMat2dRef out = (GLMMat2dRef)GLM_ALLOCATE(GLMMat2D);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(0.0);

    return out;
}

/**
* Creates a new GLMMat2d initialized with values from an existing matrix
*
* @param a reference to matrix to clone
* @returns a new 2x3 matrix
*/
GLMMat2dRef GLMMat2dClone(GLMMat2dRef a)
{
    GLMMat2dRef out = GLMMat2dCreate();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];

    return out;
}

/**
* Copies the values from one GLMMat2d to another
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat2dCopy(__OUT__ GLMMat2dRef out, GLMMat2dRef a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
}

/**
* Set a GLMMat2d to the identity matrix
*
* @param out reference to the receiving matrix
*/
void GLMMat2dIdentity(__OUT__ GLMMat2dRef out)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(0.0);
}

/**
* Create a new GLMMat2d with the given values
*
* @param a Component A (index 0)
* @param b Component B (index 1)
* @param c Component C (index 2)
* @param d Component D (index 3)
* @param tx Component TX (index 4)
* @param ty Component TY (index 5)
* @returns A new GLMMat2d
*/
GLMMat2dRef GLMMat2dFromValues(GLMRational a, GLMRational b, GLMRational c, GLMRational d, GLMRational tx, GLMRational ty)
{
    GLMMat2dRef out = GLMMat2dCreate();
    out[0] = a;
    out[1] = b;
    out[2] = c;
    out[3] = d;
    out[4] = tx;
    out[5] = ty;

    return out;
}

/**
* Set the components of a GLMMat2d to the given values
*
* @param out reference to the receiving matrix
* @param a Component A (index 0)
* @param b Component B (index 1)
* @param c Component C (index 2)
* @param d Component D (index 3)
* @param tx Component TX (index 4)
* @param ty Component TY (index 5)
*/
void GLMMat2dSet(__OUT__ GLMMat2dRef out, GLMRational a, GLMRational b, GLMRational c, GLMRational d, GLMRational tx, GLMRational ty)
{
    out[0] = a;
    out[1] = b;
    out[2] = c;
    out[3] = d;
    out[4] = tx;
    out[5] = ty;
}

/**
* Inverts a GLMMat2d
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat2dInvert(__OUT__ __NULLABLE__ GLMMat2dRef out, GLMMat2dRef a)
{
    GLMRational aa = a[0], ab = a[1], ac = a[2], ad = a[3],
        atx = a[4], aty = a[5];

    //Computing determinant
    GLMRational det = aa * ad - ab * ac;

    if (det == GLM_RATIONAL(0.0)) {
        out = NULL;
        return;
    }

    det = GLM_RATIONAL(1.0) / det;

    out[0] = ad * det;
    out[1] = -ab * det;
    out[2] = -ac * det;
    out[3] = aa * det;
    out[4] = (ac * aty - ad * atx) * det;
    out[5] = (ab * atx - aa * aty) * det;
}

/**
* Calculates the determinant of a GLMmat2d
*
* @param a reference to the source matrix
* @returns determinant of a
*/
GLMRational GLMMat2dDeterminant(GLMMat2dRef a)
{
    return a[0] * a[3] - a[1] * a[2];
}

/**
* Multiplies two GLMMat2d's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2dMultiply(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMMat2dRef b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
                b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];

    out[0] = a0 * b0 + a2 * b1;
    out[1] = a1 * b0 + a3 * b1;
    out[2] = a0 * b2 + a2 * b3;
    out[3] = a1 * b2 + a3 * b3;
    out[4] = a0 * b4 + a2 * b5 + a4;
    out[5] = a1 * b4 + a3 * b5 + a5;
}

/**
* Rotates a GLMMat2d by the given angle
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat2dRotate(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMRational rad)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
                s = GLM_SIN(rad),
                c = GLM_COS(rad);

    out[0] = a0 *  c + a2 * s;
    out[1] = a1 *  c + a3 * s;
    out[2] = a0 * -s + a2 * c;
    out[3] = a1 * -s + a3 * c;
    out[4] = a4;
    out[5] = a5;
}

/**
* Scales the GLMMat2d by the dimensions in the given GLMVec2
*
* @param out reference to  the receiving matrix
* @param a reference to the matrix to translate
* @param v reference to the GLMVec2 to scale the matrix by
**/
void GLMMat2dScale(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMVec2Ref v)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
                v0 = v[0], v1 = v[1];

    out[0] = a0 * v0;
    out[1] = a1 * v0;
    out[2] = a2 * v1;
    out[3] = a3 * v1;
    out[4] = a4;
    out[5] = a5;
}

/**
* Translates the GLMMat2d by the dimensions in the given GLMVec2
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to translate
* @param v reference to the vec2 to translate the matrix by
**/
void GLMMat2dTranslate(__OUT__ GLMMat2dRef out, GLMMat2Ref a, GLMVec2Ref v)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5],
                v0 = v[0], v1 = v[1];

    out[0] = a0;
    out[1] = a1;
    out[2] = a2;
    out[3] = a3;
    out[4] = a0 * v0 + a2 * v1 + a4;
    out[5] = a1 * v0 + a3 * v1 + a5;
}

/**
* Sets a matrix from a given angle
*
* @param out reference to mat2d receiving operation result
* @param rad reference to the angle to rotate the matrix by
*/
void GLMMat2dFromRotation(__OUT__ GLMMat2dRef out, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad), c = GLM_COS(rad);

    out[0] = c;
    out[1] = s;
    out[2] = -s;
    out[3] = c;
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(0.0);
}

/**
* Sets a matrix from a vector scaling
* 
* @param out GLMMat2d receiving operation result
* @param v Scaling vector
*/
void GLMMat2dFromScaling(__OUT__ GLMMat2dRef out, GLMVec2Ref v)
{
    out[0] = v[0];
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = v[1];
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(0.0);
}

/**
* Creates a matrix from a vector translation
*
* @param out GLMMat2d receiving operation result
* @param v Translation vector
*/
void GLMMat2dFromTranslation(__OUT__ GLMMat2dRef out, GLMVec2Ref v)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
    out[4] = v[0];
    out[5] = v[1];
}

/**
* Writes string representations for GLMMat2d in out parameter.
* out must be at least 64 bytes long.
*
* @param out recieving string buffer
* @param a reference to matrix to represent as a string
*/
void GLMMat2dStr(__OUT__ char *const out, GLMMat2dRef a)
{
    sprintf_s(out, 64, "GLMMat2d(%f, %f, %f, %f, %f, %f)", a[0], a[1], a[2], a[3], a[4], a[5]);
}

/**
* Returns Frobenius norm of a GLMMat2d
*
* @param a reference to the matrix to calculate Frobenius norm of
* @returns Frobenius norm
*/
GLMRational GLMMat2dFrob(GLMMat2dRef a)
{
    return(GLM_SQRT(GLM_POW(a[0], 2) + GLM_POW(a[1], 2) + GLM_POW(a[2], 2) + GLM_POW(a[3], 2) + GLM_POW(a[4], 2) + GLM_POW(a[5], 2) + 1));
}

/**
* Adds two GLMMat2d's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2dAdd(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMMat2dRef b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
    out[4] = a[4] + b[4];
    out[5] = a[5] + b[5];
}

/**
* Subtracts matrix b from matrix a
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2dSubtract(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMMat2dRef b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
    out[4] = a[4] - b[4];
    out[5] = a[5] - b[5];
}

/**
* Multiply each element of the matrix by a scalar.
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to scale
* @param b amount to scale the matrix's elements by
*/
void GLMMat2dMultiplyScalar(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
    out[4] = a[4] * b;
    out[5] = a[5] * b;
}

/**
* Adds two GLMMat2d's after multiplying each element of the second operand by a scalar value.
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b's elements by before adding
*/
void GLMMat2dMultiplyScalarAndAdd(__OUT__ GLMMat2dRef out, GLMMat2dRef a, GLMMat2dRef b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
    out[4] = a[4] + (b[4] * scale);
    out[5] = a[5] + (b[5] * scale);
}

/**
* Returns whether or not the matrices have exactly the same values.
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat2dExactEquals(GLMMat2dRef a, GLMMat2dRef b)
{
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3] && a[4] == b[4] && a[5] == b[5];
}


/**
* Returns whether or not the matrices have approximately the same elements in the same position.
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat2dEquals(GLMMat2dRef a, GLMMat2dRef b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3], a4 = a[4], a5 = a[5];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3], b4 = b[4], b5 = b[5];

    return  GLM_ABS(a0 - b0) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a0), GLM_ABS(b0)) ) &&
            GLM_ABS(a1 - b1) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a1), GLM_ABS(b1)) ) &&
            GLM_ABS(a2 - b2) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a2), GLM_ABS(b2)) ) &&
            GLM_ABS(a3 - b3) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a3), GLM_ABS(b3)) ) &&
            GLM_ABS(a4 - b4) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a4), GLM_ABS(b4)) ) &&
            GLM_ABS(a5 - b5) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a5), GLM_ABS(b5)) );
}

#endif