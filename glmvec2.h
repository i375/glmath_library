/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmvec2_h
#define glmath_glmvec2_h

#include "common.h"



/**
* Creates a new, empty GLMVec2
*
* @returns a new 2D vector
*/
GLMVec2Ref GLMVec2Create()
{
    GLMVec2Ref a = (GLMVec2Ref)GLM_ALLOCATE(GLMVec2);

    if (a == NULL)
    {
        return NULL;
    }

    a[0] = GLM_RATIONAL(0.0);
    a[1] = GLM_RATIONAL(0.0);

    return a;
}

/**
* Creates a new GLMVec2 initialized with values from an existing vector
*
* @param a vector to clone
* @returns a new 2D vector
*/
GLMVec2Ref GLMVec2Clone(GLMVec2Ref a)
{
    GLMVec2Ref b = GLMVec2Create();

    b[0] = a[0];
    b[1] = a[1];

    return b;
}

/**
* Creates a new GLMVec2 initialized with the given values
*
* @param x X component
* @param y Y component
* @returns a new 2D vector
*/
GLMVec2Ref GLMVec2FromValues(GLMRational x, GLMRational y)
{
    GLMVec2Ref vec2 = GLMVec2Create();

    vec2[0] = x;
    vec2[1] = y;

    return vec2;

}

/**
* Copy the values from one GLMVec2 to another
*
* @param out the receiving vector
* @param a the source vector
*/
void GLMVec2Copy(__OUT__ GLMVec2Ref out, GLMVec2Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
}

/**
* Set the components of a GLMVec2 to the given values
*
* @param out the receiving vector
* @param x X component
* @param y Y component
*/
void GLMVec2Set(__OUT__ GLMVec2Ref out, GLMRational x, GLMRational y)
{
    out[0] = x;
    out[1] = y;
}

/**
* Adds two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Add(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
}

/**
* Subtracts vector b from vector a
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Subtract(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
}

/**
* Multiplies two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Multiply(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
}

/**
* Divides two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Divide(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
}


/**
* Returns the minimum of two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Min(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = fminf(a[0], b[0]);
    out[1] = fminf(a[1], b[1]);
}


/**
* Returns the maximum of two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Max(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    out[0] = GLM_MAX(a[0], b[0]);
    out[1] = GLM_MAX(a[1], b[1]);
}

/**
* Scales a vec2 by a scalar number
*
* @param out the receiving vector
* @param a the vector to scale
* @param b amount to scale the vector by
*/
void GLMVec2Scale(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
}


/**
* Adds two GLMVec2's after scaling the second operand by a scalar value
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
* @param scale the amount to scale b by before adding
*/
void GLMVec2ScaleAndAdd(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
}

/**
* Calculates the euclidian distance between two vec2's
*
* @param a the first operand
* @param b the second operand
* @returns distance between a and b
*/
GLMRational GLMVec2Distance(GLMVec2Ref a, GLMVec2Ref b)
{
    GLMRational x = b[0] - a[0],
                y = b[1] - a[1];

    return GLM_SQRT(x*x + y*y);
}

/**
* Calculates the squared euclidian distance between two vec2's
*
* @param a the first operand
* @param b the second operand
* @returns squared distance between a and b
*/
GLMRational GLMVec2SquaredDistance(GLMVec2Ref a, GLMVec2Ref b)
{
    GLMRational x = b[0] - a[0],
                y = b[1] - a[1];

    return x*x + y*y;
}


/**
* Calculates the length of a GLMVec2
*
* @param a vector to calculate length of
* @returns length of a
*/
GLMRational GLMVec2Length(GLMVec2Ref a)
{
    GLMRational x = a[0],
                y = a[1];

    return GLM_SQRT(x*x + y*y);
}

/**
* Calculates the squared length of a GLMVec2
*
* @param a vector to calculate squared length of
* @returns squared length of a
*/
GLMRational GLMVec2QuaredLength(GLMVec2Ref a)
{
    GLMRational x = a[0],
                y = a[1];

    return x * x + y * y;
}

/**
* Returns the inverse of the components of a GLMVec2
*
* @param out the receiving vector
* @param a vector to invert
*/
void GLMVec2Negate(__OUT__ GLMVec2Ref out, GLMVec2Ref a)
{
    out[0] = GLM_RATIONAL(1.0) / a[0];
    out[1] = GLM_RATIONAL(1.0) / a[1];
}

/**
* Normalize a GLMVec2
*
* @param out the receiving vector
* @param a vector to normalize
*/
void GLMVec2Normilize(__OUT__ GLMVec2Ref out, GLMVec2Ref a)
{

    GLMRational x = a[0],
                y = a[1];

    GLMRational len = x*x + y*y;

    if (len > 0) 
    {
        len = 1 / GLM_SQRT(len);

        out[0] = a[0] * len;
        out[1] = a[1] * len;
    }

}

/**
* Calculates the dot product of two GLMVec2's
*
* @param a the first operand
* @param b the second operand
* @returns dot product of a and b
*/
GLMRational GLMVec2Dot(GLMVec2Ref a, GLMVec2Ref b)
{
    return a[0] * b[0] + a[1] * b[1];
}

/**
* Computes the cross product of two GLMVec2's
* Note that the cross product must by definition produce a 3D vector
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
*/
void GLMVec2Cross(__OUT__ GLMVec3Ref out, GLMVec2Ref a, GLMVec2Ref b)
{
    GLMRational z = a[0] * b[1] - a[1] * b[0];

    out[0] = out[1] = GLM_RATIONAL(0.0);

    out[2] = z;
}

/**
* Performs a linear interpolation between two GLMVec2's
*
* @param out the receiving vector
* @param a the first operand
* @param b the second operand
* @param t interpolation amount between the two inputs
*/
void GLMVec2Lerp(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMVec2Ref b, GLMRational t)
{
    GLMRational ax = a[0],
                ay = a[1];

    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
}

/**
* Generates a random vector with the given scale
*
* @param out the receiving vector
* @param [scale] Length of the resulting vector.
*/
void GLMVec2Random(__OUT__ GLMVec2Ref out, GLMRational scale)
{
    GLMRational r = GLMRandom() * 2.0F * GLM_PI;

    out[0] = GLM_COS(r) * scale;
    out[1] = GLM_SIN(r) * scale;
}

/**
* Transforms the GLMVec2 with a GLMMat2
*
* @param out the receiving vector
* @param a the vector to transform
* @param m matrix to transform with
*/
void GLMVec2TransformMat2(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMMat2Ref m)
{
    GLMRational x = a[0],
                y = a[1];

    out[0] = m[0] * x + m[2] * y;
    out[1] = m[1] * x + m[3] * y;
}

/**
* Transforms the GLMVec2 with a GLMMat2d
*
* @param out the receiving vector
* @param a the vector to transform
* @param m matrix to transform with
*/
void GLMVec2TransformMat2d(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMMat2dRef m)
{
    GLMRational x = a[0],
                y = a[1];

    out[0] = m[0] * x + m[2] * y + m[4];
    out[1] = m[1] * x + m[3] * y + m[5];
}

/**
* Transforms the GLMVec2 with a GLMMat3
* 3rd vector component is implicitly '1'
*
* @param out the receiving vector
* @param a the vector to transform
* @param m matrix to transform with
*/
void GLMVec2TransformMat3(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMMat3Ref m)
{
    GLMRational x = a[0],
                y = a[1];

    out[0] = m[0] * x + m[3] * y + m[6];
    out[1] = m[1] * x + m[4] * y + m[7];
}

/**
* Transforms the GLMVec2 with a GLMMat4
* 3rd vector component is implicitly '0'
* 4th vector component is implicitly '1'
*
* @param out the receiving vector
* @param a the vector to transform
* @param m matrix to transform with
*/
void TransformMat4(__OUT__ GLMVec2Ref out, GLMVec2Ref a, GLMMat4Ref m)
{
    GLMRational x = a[0],
                y = a[1];

    out[0] = m[0] * x + m[4] * y + m[12];
    out[1] = m[1] * x + m[5] * y + m[13];
}

/**
* Writes a string representation of a vector in out buffer.
*
* @param char* buffer at least 64 chars long.
* @param a vector to represent as a string
*/
void GLMVec2Str(__OUT__ char *const out, GLMVec2Ref a)
{
    sprintf_s(out, 64, "GLMVec2(%f, %f)", a[0], a[1]);
}

/**
* Returns whether or not the vectors exactly have the same values 
*
* @param a The first vector.
* @param b The second vector.
* @returns True if the vectors are equal, false otherwise.
*/
bool GLMVec2ExactEquals(GLMVec2Ref a, GLMVec2Ref b)
{
    return a[0] == b[0] && a[1] == b[1];
}

/**
* Returns whether or not the vectors have approximately the same elements in the same position.
*
* @param a The first vector.
* @param b The second vector.
* @returns true if the vectors are equal, false otherwise.
*/
bool GLMVec2Equals(GLMVec2Ref a, GLMVec2Ref b)
{
    GLMRational a0 = a[0], a1 = a[1];
    GLMRational b0 = b[0], b1 = b[1];

    return ( GLM_ABS(a0 - b0) <= GLM_EPSILON * GLM_MAX(1.0, GLM_MAX(GLM_ABS(a0), GLM_ABS(b0))) &&
             GLM_ABS(a1 - b1) <= GLM_EPSILON * GLM_MAX(1.0, GLM_MAX(GLM_ABS(a1), GLM_ABS(b1))) );
}

#endif