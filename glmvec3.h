/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmvec3_h
#define glmath_glmvec3_h

#include "common.h"

/**
* Creates a new, empty GLMVec3
*
* @returns Reference to a new 3D vector
*/
GLMVec3Ref GLMVec3Create()
{
    GLMVec3Ref out = (GLMVec3Ref)GLM_ALLOCATE(GLMVec3);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(0.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);

    return out;
}


/**
* Creates a new GLMVec3 initialized with values from an existing vector
*
* @param Reference to a vector to clone
* @returns Reference to a new 3D vector
*/
GLMVec3Ref GLMVec3Clone(GLMVec3Ref a)
{
    GLMVec3Ref out = GLMVec3Create();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];

    return out;
}

/**
* Creates a new GLMVec3 initialized with the given values
*
* @param x X component
* @param y Y component
* @param z Z component
* @returns Reference to a new 3D vector
*/
GLMVec3Ref GLMVec3FromValues(GLMRational x, GLMRational y, GLMRational z)
{
    GLMVec3Ref out = GLMVec3Create();

    out[0] = x;
    out[1] = y;
    out[2] = z;

    return out;
}

/**
* Copy the values from one GLMVec3 to another
*
* @param out reference to the receiving vector
* @param a reference to the source vector
*/
void GLMVec3Copy(__OUT__ GLMVec3Ref out, GLMVec3Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
}

/**
* Set the components of a GLMVec3 to the given values
*
* @param out reference to the receiving vector
* @param x X component
* @param y Y component
* @param z Z component
*/
void GLMVec3Set(__OUT__ GLMVec3Ref out, GLMRational x, GLMRational y, GLMRational z)
{
    out[0] = x;
    out[1] = y;
    out[2] = z;
}

/**
* Adds two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec3Add(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
}

/**
* Subtracts vector b from vector a
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec3Subtract(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
}

/**
* Multiplies two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @returns out
*/
void GLMVec3Multiply(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = a[0] * b[0];
    out[1] = a[1] * b[1];
    out[2] = a[2] * b[2];
}

/**
* Divides two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec3Divide(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = a[0] / b[0];
    out[1] = a[1] / b[1];
    out[2] = a[2] / b[2];
}


/**
* Returns the minimum of two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @returns out
*/
void GLMVec3Min(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = GLM_MIN(a[0], b[0]);
    out[1] = GLM_MIN(a[1], b[1]);
    out[2] = GLM_MIN(a[2], b[2]);
}

/**
* Returns the maximum of two GLMVec3's
*
* @param out the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec3Max(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    out[0] = GLM_MAX(a[0], b[0]);
    out[1] = GLM_MAX(a[1], b[1]);
    out[2] = GLM_MAX(a[2], b[2]);
}

/**
* Scales a GLMVec3 by a scalar number
*
* @param out the receiving vector
* @param a reference to the vector to scale
* @param b reference to amount to scale the vector by
*/
void GLMVec3Scale(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
}

/**
* Adds two GLMVec3's after scaling the second operand by a scalar value
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b by before adding
*/
void GLMVec3ScaleAndAdd(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
}

/**
* Calculates the euclidian distance between two GLMVec3's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns distance between a and b
*/
GLMRational GLMVec3Distance(GLMVec3Ref a, GLMVec3Ref b)
{
    GLMRational  x = b[0] - a[0],
                 y = b[1] - a[1],
                 z = b[2] - a[2];
    
    return GLM_SQRT(x*x + y*y + z*z);
}


/**
* Calculates the squared euclidian distance between two GLMVec3's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns squared distance between a and b
*/
GLMRational GLMVec3SquaredDistance(GLMVec3Ref a, GLMVec3Ref b)
{
    GLMRational  x = b[0] - a[0],
                 y = b[1] - a[1],
                 z = b[2] - a[2];

    return x*x + y*y + z*z;
}

/**
* Calculates the length of a GLMVec3
*
* @param a reference to vector to calculate length of
* @returns length of a
*/
GLMRational GLMVec3Length(GLMVec3Ref a)
{
    GLMRational  x = a[0],
                 y = a[1],
                 z = a[2];

    return GLM_SQRT(x*x + y*y + z*z);
}

/**
* Calculates the squared length of a GLMVec3
*
* @param a reference to vector to calculate squared length of
* @returns squared length of a
*/
GLMRational GLMVec3SquaredLength(GLMVec3Ref a)
{
    GLMRational  x = a[0],
                 y = a[1],
                 z = a[2];

    return x*x + y*y + z*z;
}

/**
* Negates the components of a GLMVec3
*
* @param out reference to the receiving vector
* @param a vector to negate
*/
void GLMVec3Negate(__OUT__ GLMVec3Ref out, GLMVec3Ref a) 
{
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
}


/**
* Returns the inverse of the components of a GLMVec3
*
* @param out the receiving vector
* @param a vector to invert
*/
void GLMVec3Inverse(__OUT__ GLMVec3Ref out, GLMVec3Ref a)
{
    out[0] = GLM_RATIONAL(1.0) / a[0];
    out[1] = GLM_RATIONAL(1.0) / a[1];
    out[2] = GLM_RATIONAL(1.0) / a[2];
}

/**
* Normalize a GLMVec3
*
* @param out reference to the receiving vector
* @param a reference to vector to normalize
*/
void GLMVec3Normilize(__OUT__ GLMVec3Ref out, GLMVec3Ref a)
{
    GLMRational x = a[0],
                y = a[1],
                z = a[2];

    GLMRational len = x*x + y*y + z*z;

    if (len > 0) 
    {
        len = 1 / GLM_SQRT(len);
        out[0] = a[0] * len;
        out[1] = a[1] * len;
        out[2] = a[2] * len;
    }
}

/**
* Calculates the dot product of two GLMVec3's
*
* @param a reference to the first operand
* @param b reference to the second operand
* @returns dot product of a and b
*/
GLMRational GLMVec3Dot(GLMVec3Ref a, GLMVec3Ref b)
{
    return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

/**
* Computes the cross product of two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMVec3Cross(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b)
{
    GLMRational ax = a[0], ay = a[1], az = a[2],
                bx = b[0], by = b[1], bz = b[2];

    out[0] = ay * bz - az * by;
    out[1] = az * bx - ax * bz;
    out[2] = ax * by - ay * bx;
}

/**
* Performs a linear interpolation between two GLMVec3's
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param t interpolation amount between the two inputs
*/
void GLMVec3Lerp(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMRational t)
{
    GLMRational ax = a[0],
                ay = a[1],
                az = a[2];

    out[0] = ax + t * (b[0] - ax);
    out[1] = ay + t * (b[1] - ay);
    out[2] = az + t * (b[2] - az);
}

/**
* Performs a hermite interpolation with two control points
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param c reference to the third operand
* @param d reference to the fourth operand
* @param t interpolation amount between the two inputs
*/
void GLMVec3Hermite(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMVec3Ref c, GLMVec3Ref d, GLMRational t)
{
    GLMRational factorTimes2 = t * t,
                factor1 = factorTimes2 * (2 * t - 3) + 1,
                factor2 = factorTimes2 * (t - 2) + t,
                factor3 = factorTimes2 * (t - 1),
                factor4 = factorTimes2 * (3 - 2 * t);

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;
}

/**
* Performs a bezier interpolation with two control points
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param c reference to the third operand
* @param d reference to the fourth operand
* @param t interpolation amount between the two inputs
*/
void GLMVec3Bezier(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMVec3Ref c, GLMVec3Ref d, GLMRational t)
{
    GLMRational inverseFactor = 1 - t,
                inverseFactorTimesTwo = inverseFactor * inverseFactor,
                factorTimes2 = t * t,
                factor1 = inverseFactorTimesTwo * inverseFactor,
                factor2 = 3 * t * inverseFactorTimesTwo,
                factor3 = 3 * factorTimes2 * inverseFactor,
                factor4 = factorTimes2 * t;

    out[0] = a[0] * factor1 + b[0] * factor2 + c[0] * factor3 + d[0] * factor4;
    out[1] = a[1] * factor1 + b[1] * factor2 + c[1] * factor3 + d[1] * factor4;
    out[2] = a[2] * factor1 + b[2] * factor2 + c[2] * factor3 + d[2] * factor4;
}

/**
* Computes a random vector with the given scale
*
* @param out reference to the receiving vector
* @param scale Length of the resulting vector. 
*/
void GLMVec3Random(__OUT__ GLMVec3Ref out, GLMRational scale)
{

    GLMRational r      = GLMRandom() * GLM_RATIONAL(2.0) * GLM_PI;
    GLMRational z      = (GLMRandom() * GLM_RATIONAL(2.0)) - GLM_RATIONAL(1.0);
    GLMRational zScale = GLM_SQRT(GLM_RATIONAL(1.0) - z*z) * scale;

    out[0] = GLM_COS(r) * zScale;
    out[1] = GLM_SIN(r) * zScale;
    out[2] = z * scale;
}


/**
* Transforms the GLMVec3 with a GLMMat4.
* 4th vector component is implicitly '1'
*
* @param out reference to the receiving vector
* @param a reference to the vector to transform
* @param m reference to matrix to transform with
*/
void GLMVec3TransformMat4(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMMat4Ref m)
{
    GLMRational x = a[0], y = a[1], z = a[2],
                w = m[3] * x + m[7] * y + m[11] * z + m[15];

    if(w == 0 || w == NAN)
    { 
        w = GLM_RATIONAL(1.0);
    }
    

    out[0] = (m[0] * x + m[4] * y + m[8] * z + m[12]) / w;
    out[1] = (m[1] * x + m[5] * y + m[9] * z + m[13]) / w;
    out[2] = (m[2] * x + m[6] * y + m[10] * z + m[14]) / w;
}

/**
* Transforms the GLMVec3 with a GLMMat4.
*
* @param out the receiving vector
* @param a the vector to transform
* @param m the 3x3 matrix to transform with
*/
void GLMVec3TransformMat3(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMMat3Ref m)
{
    GLMRational x = a[0], y = a[1], z = a[2];

    out[0] = x * m[0] + y * m[3] + z * m[6];
    out[1] = x * m[1] + y * m[4] + z * m[7];
    out[2] = x * m[2] + y * m[5] + z * m[8];
}

/**
* Transforms the GLMVec3 with a GLMQuat
*
* @param out the receiving vector
* @param a the vector to transform
* @param q quaternion to transform with
*/
void GLMVec3TransformQuat(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMQuatRef q)
{

    GLMRational x = a[0], y = a[1], z = a[2],
                qx = q[0], qy = q[1], qz = q[2], qw = q[3],

        // calculate quat * vec
    ix = qw * x + qy * z - qz * y,
    iy = qw * y + qz * x - qx * z,
    iz = qw * z + qx * y - qy * x,
    iw = -qx * x - qy * y - qz * z;

    // calculate result * inverse quat
    out[0] = ix * qw + iw * -qx + iy * -qz - iz * -qy;
    out[1] = iy * qw + iw * -qy + iz * -qx - ix * -qz;
    out[2] = iz * qw + iw * -qz + ix * -qy - iy * -qx;

}

/**
* Rotate a 3D vector around the x-axis
* @param out reference to The receiving vec3
* @param a reference to The vec3 point to rotate
* @param b reference to The origin of the rotation
* @param c reference to The angle of rotation
*/
void GLMVec3RotateX(__OUT__ GLMVec3Ref out, GLMVec2Ref a, GLMVec2 b, GLMRational c)
{
    GLMRational p[GLMVec3ElemCount], r[GLMVec3ElemCount];

    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0];
    r[1] = p[1] * GLM_COS(c) - p[2] * GLM_SIN(c);
    r[2] = p[1] * GLM_SIN(c) + p[2] * GLM_COS(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];
}

/**
* Rotate a 3D vector around the y-axis
* @param out reference to The receiving vec3
* @param a reference to The vec3 point to rotate
* @param b reference to The origin of the rotation
* @param c The angle of rotation
*/
void GLMVec3RotateY(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMRational c)
{
    GLMRational p[GLMVec3ElemCount], r[GLMVec3ElemCount];

    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[2] * GLM_SIN(c) + p[0] * GLM_COS(c);
    r[1] = p[1];
    r[2] = p[2] * GLM_COS(c) - p[0] * GLM_SIN(c);

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];
}

/**
* Rotate a 3D vector around the z-axis
* @param out reference to the receiving vec3
* @param a reference to the vec3 point to rotate
* @param b reference to the origin of the rotation
* @param c The angle of rotation
*/
void GLMVec3RotateZ(__OUT__ GLMVec3Ref out, GLMVec3Ref a, GLMVec3Ref b, GLMRational c)
{
    GLMRational p[GLMVec3ElemCount], r[GLMVec3ElemCount];

    //Translate point to the origin
    p[0] = a[0] - b[0];
    p[1] = a[1] - b[1];
    p[2] = a[2] - b[2];

    //perform rotation
    r[0] = p[0] * GLM_COS(c) - p[1] * GLM_SIN(c);
    r[1] = p[0] * GLM_SIN(c) + p[1] * GLM_COS(c);
    r[2] = p[2];

    //translate to correct position
    out[0] = r[0] + b[0];
    out[1] = r[1] + b[1];
    out[2] = r[2] + b[2];
}

/**
* Get the angle between two 3D vectors
* @param a reference to The first operand
* @param b reference to The second operand
* @returns The angle in radians
*/
GLMRational GLMVec3Angle(GLMVec3Ref a, GLMVec3Ref b)
{
    GLMVec3 tempA;
    GLMVec3Set(tempA, a[0], a[1], a[2]);

    GLMVec3 tempB; 
    GLMVec3Set(tempB, b[0], b[1], b[2]);

    GLMVec3Normilize(tempA, tempA);
    GLMVec3Normilize(tempB, tempB);

    GLMRational cosine = GLMVec3Dot(tempA, tempB);

    if (cosine > 1.0) 
    {
        return GLM_RATIONAL(0.0);
    }
    else 
    {
        return GLM_COS(cosine);
    }
}

/**
* Writes string representations for GLMVec3 in out parameter.
* out must be at least 64 bytes long.
*
* @param out recieving string buffer
* @param a reference to vector to represent as a string
*/
void GLMVec3Str(__OUT__ char *const out, GLMVec3Ref a)
{
    sprintf_s(out, 64, "GLMVec3(%f, %f, %f)", a[0], a[1], a[2]);
}

/**
* Returns whether or not the vectors have exactly the same elements in the same position
*
* @param a reference to The first vector.
* @param b reference to The second vector.
* @returns True if the vectors are equal, false otherwise.
*/
bool GLMVec3ExactEquals(GLMVec3Ref a, GLMVec3Ref b)
{
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
}

/**
* Returns whether or not the vectors have approximately the same elements in the same position.
*
* @param a reference to The first vector.
* @param b reference to The second vector.
* @returns True if the vectors are equal, false otherwise.
*/
bool GLMVec3Equals(GLMVec3Ref a, GLMVec3Ref b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2];

    return (GLM_ABS(a0 - b0) <= GLM_EPSILON * GLM_MAX(1.0, GLM_MAX((a0), (b0)) ) &&
            GLM_ABS(a1 - b1) <= GLM_EPSILON * GLM_MAX(1.0, GLM_MAX((a1), (b1)) ) &&
            GLM_ABS(a2 - b2) <= GLM_EPSILON * GLM_MAX(1.0, GLM_MAX((a2), (b2)) ));
}

#endif