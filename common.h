/*
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdbool.h>

#ifndef glmath_common_h
#define glmath_common_h

#ifndef __OUT__
#define __OUT__
#endif

#ifndef __IN_OUT__
#define __IN_OUT__
#endif

#ifndef __NULLABLE__
#define __NULLABLE__
#endif


//Precision constants
#define GLM_PRECISION_FLOAT 1
#define GLM_PRECISION_DOUBLE 2


//Setting precision
#define GLM_PRECISION GLM_PRECISION_FLOAT // or GLM_PRECISION_DOUBLE


#if GLM_PRECISION == GLM_PRECISION_FLOAT
typedef float GLMRational;
#endif

#if GLM_PRECISION == GLM_PRECISION_DOUBLE
typedef double GLMRational;
#endif


//Mapping to proper standard functions and suffixes
#if GLM_PRECISION == GLM_PRECISION_FLOAT
#define GLM_SIN(x)       sinf(x)
#define GLM_COS(x)       cosf(x)
#define GLM_ACOS(x)      acosf(x)
#define GLM_TAN(x)       tanf(x)

#define GLM_SQRT(x)      sqrtf(x)
#define GLM_POW(x,y)     powf(x,y)

#define GLM_ABS(x)       fabsf(x)

#define GLM_MAX(x, y)    fmaxf(x, y)
#define GLM_MAX_3(x,y,z) fmaxf(x, fmaxf(y,z))
#define GLM_MIN(x, y)    fminf(x, y)

#define GLM_RATIONAL(x)  x##F
#endif

#if GLM_PRECISION == GLM_PRECISION_DOUBLE
#define GLM_SIN(x)       sin(x)
#define GLM_COS(x)       cos(x)
#define GLM_ACOS(x)      acos(x)
#define GLM_TAN(x)       tan(x)

#define GLM_SQRT(x)      sqrt(x)
#define GLM_POW(x,y)     pow(x,y)

#define GLM_ABS(x)       abs(x)

#define GLM_MAX(x, y)    fmax(x,y)
#define GLM_MAX_3(x,y,z) fmax(x, fmax(y,z))
#define GLM_MIN(x, y)    min(x, y)

#define GLM_RATIONAL(x)  x
#endif

const GLMRational GLM_PI      = GLM_RATIONAL(3.14159265358979323846);
const GLMRational GLM_EPSILON = GLM_RATIONAL(0.00000001);
const GLMRational GLM_DEGREE  = GLM_PI / GLM_RATIONAL(180.0);

//Reference type definitions
typedef GLMRational* GLMVec2Ref;
typedef GLMRational* GLMVec3Ref;
typedef GLMRational* GLMVec4Ref;
typedef GLMRational* GLMMat2Ref;
typedef GLMRational* GLMMat2dRef;
typedef GLMRational* GLMMat3Ref;
typedef GLMRational* GLMMat4Ref;
typedef GLMRational* GLMQuatRef;

const int GLMVec2ElemCount  = 2;
const int GLMVec3ElemCount  = 3;
const int GLMVec4ElemCount  = 4;
const int GLMMat2ElemCount  = 2 * 2;
const int GLMMAt2DElemCount = 2 * 3;
const int GLMMat3ElemCount  = 3 * 3;
const int GLMMat4ElemCount  = 4 * 4;
const int GLMQuatElemCount  = 4;

const int GLMRationalByteCount = sizeof(GLMRational);

const int GLMVec2ByteCount  = GLMRationalByteCount * GLMVec2ElemCount;
const int GLMVec3ByteCount  = GLMRationalByteCount * GLMVec3ElemCount;
const int GLMVec4ByteCount  = GLMRationalByteCount * GLMVec4ElemCount;
const int GLMMat2ByteCount  = GLMRationalByteCount * GLMMat2ElemCount;
const int GLMMat2DByteCount = GLMRationalByteCount * GLMMAt2DElemCount;
const int GLMMat3ByteCount  = GLMRationalByteCount * GLMMat3ElemCount;
const int GLMMat4ByteCount  = GLMRationalByteCount * GLMMat4ElemCount;
const int GLMQuatByteCount  = GLMRationalByteCount * GLMQuatElemCount;

//Type definitions for stack allocations
typedef GLMRational GLMVec2[GLMVec2ElemCount];
typedef GLMRational GLMVec3[GLMVec3ElemCount];
typedef GLMRational GLMVec4[GLMVec4ElemCount];
typedef GLMRational GLMMat2[GLMMat2ElemCount];
typedef GLMRational GLMMat2D[GLMMAt2DElemCount];
typedef GLMRational GLMMat3[GLMMat3ElemCount];
typedef GLMRational GLMMat4[GLMMat4ElemCount];
typedef GLMRational GLMQuat[GLMQuatElemCount];

struct GLMFOV4
{
    GLMRational upDegrees;
    GLMRational downDegrees;
    GLMRational leftDegrees;
    GLMRational rightDegrees;
};

typedef GLMFOV4* GLMFOV4Ref;


bool GLMEquals(GLMRational a, GLMRational b)
{
    GLMRational r = GLM_RATIONAL(3.0);

    return GLM_ABS(a - b) <= GLM_EPSILON * GLM_MAX_3(1.0F, GLM_ABS(a), GLM_ABS(b) );
}

inline float GLMRandom()
{
    return (GLMRational)rand() / (GLMRational)RAND_MAX;
}

#define GLM_ALLOCATE(type) malloc(sizeof(type))
#define GLM_FREE(ref) free(ref)

#endif