/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmquat_h
#define glmath_glmquat_h

#include "common.h"

#define GLMQuatNormalize GLMVec4Normalize
#define GLMQuatClone GLMVec4Clone
#define GLMQuatFromValues GLMVec4FromValues
#define GLMQuatCopy GLMVec4Copy
#define GLMQueatSet GLMVec4Set
#define GLMQuatAdd GLMVec4Add
#define GLMQuatScale GLMVec4Scale
#define GLMQuatDot GLMVec4Dot
#define GLMQuatLerp GLMVec4Lerp
#define GLMQuatLength GLMVec4Length
#define GLMQuatSuaredLength GLMVec4SquaredLength
#define GLMQuatExactEquals GLMVec4ExactEquals
#define GLMEquals GLMVec4Equals

void GLMQuatSetAxisAngle(__IN_OUT__ GLMQuatRef out, GLMVec3Ref axis, GLMRational rad);
void GLMQuatFromMat3(__OUT__ GLMQuatRef out, GLMMat3Ref a);

/**
* Creates a new identity quat
*
* @returns a reference to new quaternion
*/
GLMQuatRef GLMQuatCreate()
{
    GLMQuatRef out = (GLMQuatRef)GLM_ALLOCATE(GLMQuat);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(0.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);

    return out;
}

/**
* Sets a quaternion to represent the shortest rotation from one
* vector to another.
*
* Both vectors are assumed to be unit length.
*
* @param out reference to the receiving quaternion.
* @param a the initial vector
* @param b the destination vector
*/
void GLMQuatRotateTo(__OUT__ GLMQuatRef out, GLMVec3Ref a, GLMVec3Ref b)
{
    GLMVec3 tmpvec3;

    GLMVec3 xUnitVec3 = { GLM_RATIONAL(1.0), GLM_RATIONAL(0.0), GLM_RATIONAL(0.0) };
    GLMVec3 yUnitVec3 = { GLM_RATIONAL(0.0), GLM_RATIONAL(1.0), GLM_RATIONAL(0.0) };

    GLMRational dot = GLMVec3Dot(a, b);

    if (dot < -GLM_RATIONAL(0.999999)) 
    {
        GLMVec3Cross(tmpvec3, xUnitVec3, a);

        if (GLMVec3Length(tmpvec3) < GLM_RATIONAL(0.000001) )
        {
            GLMVec3Cross(tmpvec3, yUnitVec3, a);
        }
            
        GLMVec3Normilize(tmpvec3, tmpvec3);
        GLMQuatSetAxisAngle(out, tmpvec3, GLM_PI);

        return;
    }
    else if (dot > GLM_RATIONAL(0.999999) )
    {
        out[0] = GLM_RATIONAL(0.0);
        out[1] = GLM_RATIONAL(0.0);
        out[2] = GLM_RATIONAL(0.0);
        out[3] = GLM_RATIONAL(1.0);

        return;
    }
    else 
    {
        GLMVec3Cross(tmpvec3, a, b);
        out[0] = tmpvec3[0];
        out[1] = tmpvec3[1];
        out[2] = tmpvec3[2];
        out[3] = GLM_RATIONAL(1.0) + dot;
        GLMQuatNormalize(out, out);

        return;
    }
}

/**
* Sets the specified quaternion with values corresponding to the given
* axes. Each axis is a vec3 and is expected to be unit length and
* perpendicular to all other specified axes.
*
* @param out reference to the receiving quaternion
* @param view  the vector representing the viewing direction
* @param right the vector representing the local "right" direction
* @param up    the vector representing the local "up" direction
*/
void GLMQuatSetAxes(__OUT__ GLMQuatRef out, GLMVec3Ref view, GLMVec3Ref right, GLMVec3Ref up)
{
    GLMMat3 matr;

    matr[0] = right[0];
    matr[3] = right[1];
    matr[6] = right[2];

    matr[1] = up[0];
    matr[4] = up[1];
    matr[7] = up[2];

    matr[2] = -view[0];
    matr[5] = -view[1];
    matr[8] = -view[2];

    GLMQuatFromMat3(out, matr);

    GLMQuatNormalize(out, out);
}

/**
* Set a quat to the identity quaternion
*
* @param out reference to the receiving quaternion
*/
void GLMQuatIdentity(__IN_OUT__ GLMQuatRef out)
{
    out[0] = GLM_RATIONAL(0.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
}

/**
* Sets a quat from the given angle and rotation axis,
* then returns it.
*
* @param out reference to the receiving quaternion
* @param axis reference to the axis around which to rotate
* @param rad the angle in radians
**/
void GLMQuatSetAxisAngle(__OUT__ GLMQuatRef out, GLMVec3Ref axis, GLMRational rad)
{
    rad = rad * GLM_RATIONAL(0.5);
    GLMRational s = GLM_SIN(rad);

    out[0] = s * axis[0];
    out[1] = s * axis[1];
    out[2] = s * axis[2];
    out[3] = GLM_COS(rad);
}

/**
* Multiplies two GLMQuat's
*
* @param out reference to the receiving quaternion
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMQuatMultiply(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMQuatRef b)
{
    GLMRational ax = a[0], ay = a[1], az = a[2], aw = a[3],
                bx = b[0], by = b[1], bz = b[2], bw = b[3];

    out[0] = ax * bw + aw * bx + ay * bz - az * by;
    out[1] = ay * bw + aw * by + az * bx - ax * bz;
    out[2] = az * bw + aw * bz + ax * by - ay * bx;
    out[3] = aw * bw - ax * bx - ay * by - az * bz;
}

/**
* Rotates a quaternion by the given angle about the X axis
*
* @param out reference to GLMQuat receiving operation result
* @param a reference to GLMQuat to rotate
* @param rad angle (in radians) to rotate
*/
void GLMQuatRotateX(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMRational rad)
{
    rad *= GLM_RATIONAL(0.5);

    GLMRational ax = a[0], ay = a[1], az = a[2], aw = a[3],
                bx = GLM_SIN(rad), bw = GLM_COS(rad);

    out[0] = ax * bw + aw * bx;
    out[1] = ay * bw + az * bx;
    out[2] = az * bw - ay * bx;
    out[3] = aw * bw - ax * bx;
}

/**
* Rotates a quaternion by the given angle about the Y axis
*
* @param out reference to GLMQuat receiving operation result
* @param a reference to GLMQuat to rotate
* @param rad angle (in radians) to rotate
*/
void GLMQuatRotateY(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMRational rad)
{
    rad *= GLM_RATIONAL(0.5);

    GLMRational ax = a[0], ay = a[1], az = a[2], aw = a[3],
                by = GLM_SIN(rad), bw = GLM_COS(rad);

    out[0] = ax * bw - az * by;
    out[1] = ay * bw + aw * by;
    out[2] = az * bw + ax * by;
    out[3] = aw * bw - ay * by;
}

/**
* Rotates a quaternion by the given angle about the Z axis
*
* @param out reference to GLMQuat receiving operation result
* @param a reference to GLMQuat to rotate
* @param rad angle (in radians) to rotate
*/
void GLMQuatRotateZ(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMRational rad)
{
    rad *= GLM_RATIONAL(0.5);

    GLMRational ax = a[0], ay = a[1], az = a[2], aw = a[3],
                bz = GLM_SIN(rad), bw = GLM_COS(rad);

    out[0] = ax * bw + ay * bz;
    out[1] = ay * bw - ax * bz;
    out[2] = az * bw + aw * bz;
    out[3] = aw * bw - az * bz;
}

/**
* Computes the W component of a quat from the X, Y, and Z components.
* Assumes that quaternion is 1 unit in length.
* Any existing W component will be ignored.
*
* @param out reference to the receiving quaternion
* @param a reference to GLMQuat to calculate W component of
*/
void GLMQuatComputeW(__OUT__ GLMQuatRef out, GLMQuatRef a)
{
    GLMRational x = a[0], y = a[1], z = a[2];

    out[0] = x;
    out[1] = y;
    out[2] = z;
    out[3] = GLM_SQRT(GLM_ABS(GLM_RATIONAL(1.0) - x * x - y * y - z * z));
}

/**
* Performs a spherical linear interpolation between two quat
*
* @param out reference to the receiving quaternion
* @param a reference to the first operand (from)
* @param b reference to the second operand (to)
* @param t interpolation amount between the two inputs
*/
void GLMQuatSlerp(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMQuatRef b, GLMRational t)
{
    GLMRational ax = a[0], ay = a[1], az = a[2], aw = a[3],
                bx = b[0], by = b[1], bz = b[2], bw = b[3];

    GLMRational omega, cosom, sinom, scale0, scale1;

    // Compute cosine
    cosom = ax * bx + ay * by + az * bz + aw * bw;

    // adjust signs (if necessary)
    if (cosom < GLM_RATIONAL(0.0)) 
    {
        cosom = -cosom;
        bx = -bx;
        by = -by;
        bz = -bz;
        bw = -bw;
    }

    // Compute coefficients
    if ( (GLM_RATIONAL(1.0) - cosom) > GLM_RATIONAL(0.000001) ) 
    {
        // standard case (slerp)
        omega  = GLM_ACOS(cosom);
        sinom  = GLM_SIN(omega);
        scale0 = GLM_SIN((GLM_RATIONAL(1.0) - t) * omega) / sinom;
        scale1 = GLM_SIN(t * omega) / sinom;
    }
    else 
    {
        // "from" and "to" quaternions are very close 
        //  ... so we can do a linear interpolation
        scale0 = GLM_RATIONAL(1.0) - t;
        scale1 = t;
    }

    // calculate final values
    out[0] = scale0 * ax + scale1 * bx;
    out[1] = scale0 * ay + scale1 * by;
    out[2] = scale0 * az + scale1 * bz;
    out[3] = scale0 * aw + scale1 * bw;
}

/**
* Performs a spherical linear interpolation with two control points
*
* @param out reference to the receiving quaternion
* @param a reference to the first operand
* @param b reference to the second operand
* @param c reference to the third operand
* @param d reference to the fourth operand
* @param t interpolation amount
*/
void GLMQuatSPLerp(__OUT__ GLMQuatRef out, GLMQuatRef a, GLMQuatRef b, GLMQuatRef c, GLMQuatRef d, GLMRational t)
{
    GLMQuat tempQuat1;
    GLMQuat tempQuat2;

    GLMQuatSlerp(tempQuat1, a, d, t);
    GLMQuatSlerp(tempQuat2, b, c, t);
    GLMQuatSlerp(out, tempQuat1, tempQuat2, GLM_RATIONAL(2.0) * t * (GLM_RATIONAL(1.0) - t));
}

/**
* Computes the inverse of a quaternion
*
* @param out reference to the receiving quaternion
* @param a reference to quat to calculate inverse of
*/
void GLMQuatInvert(__OUT__ GLMQuatRef out, GLMQuatRef a)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3],
                dot = a0*a0 + a1*a1 + a2*a2 + a3*a3,
                invDot = dot != GLM_RATIONAL(0.0) ? ( GLM_RATIONAL(1.0) / dot ) : GLM_RATIONAL(0.0);

    // TODO: Would be faster to return [0,0,0,0] immediately if dot == 0

    out[0] = -a0 * invDot;
    out[1] = -a1 * invDot;
    out[2] = -a2 * invDot;
    out[3] =  a3 * invDot;
}

/**
* Computes the conjugate of a quat
* If the quaternion is normalized, this function is faster than GLMQuatInverse and produces the same result.
*
* @param out reference to the receiving quaternion
* @param a reference to quat to calculate conjugate of
*/
void GLMQuatConjugate(__OUT__ GLMQuatRef out, GLMQuatRef a)
{
    out[0] = -a[0];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] =  a[3];
}

/**
* Computes a quaternion from the given 3x3 rotation matrix.
*
* NOTE: The resultant quaternion is not normalized, so you should be sure
* to renormalize the quaternion yourself where necessary.
*
* @param out reference to the receiving quaternion
* @param m reference to rotation matrix
*/
void GLMQuatFromMat3(__OUT__ GLMQuatRef out, GLMMat3Ref m)
{
    // Algorithm in Ken Shoemake's article in 1987 SIGGRAPH course notes
    // article "Quaternion Calculus and Fast Animation".
    GLMRational fTrace = m[0] + m[4] + m[8];
    GLMRational fRoot;

    if (fTrace > GLM_RATIONAL(0.0) ) 
    {
        // |w| > 1/2, may as well choose w > 1/2
        fRoot = GLM_SQRT(fTrace + GLM_RATIONAL(1.0) );  // 2w
        out[3] = GLM_RATIONAL(0.5) * fRoot;
        fRoot = GLM_RATIONAL(0.5) / fRoot;  // 1/(4w)
        out[0] = (m[5] - m[7])*fRoot;
        out[1] = (m[6] - m[2])*fRoot;
        out[2] = (m[1] - m[3])*fRoot;
    }
    else 
    {
        // |w| <= 1/2
        int i = 0;

        if (m[4] > m[0])
        {
            i = 1;
        }
            
        if (m[8] > m[i * 3 + i])
        {
            i = 2;
        }
        
        int j = (i + 1) % 3;
        int k = (i + 2) % 3;

        fRoot  = GLM_SQRT(m[i * 3 + i] - m[j * 3 + j] - m[k * 3 + k] + GLM_RATIONAL(1.0) );
        out[i] = GLM_RATIONAL(0.5) * fRoot;
        fRoot  = GLM_RATIONAL(0.5) / fRoot;

        out[3] = (m[j * 3 + k] - m[k * 3 + j]) * fRoot;
        out[j] = (m[j * 3 + i] + m[i * 3 + j]) * fRoot;
        out[k] = (m[k * 3 + i] + m[i * 3 + k]) * fRoot;
    }
}

/**
* Writes string representations for GLMQuat in out parameter.
* out must be at least 64 bytes long.
*
* @param out recieving string buffer
* @param a reference to quaternion to represent as a string
*/
void GLMQuatStr(__OUT__ char *const out, GLMQuatRef a)
{
    sprintf_s(out, 64, "GLMQuat(%f, %f, %f, %f)", a[0], a[1], a[2], a[3]);
}

#endif