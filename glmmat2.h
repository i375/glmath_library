/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmat2_h
#define glmath_glmat2_h

#include "common.h"

/**
* Creates a new identity GLMMat2
*
* @returns a new 2x2 matrix
*/
GLMMat2Ref GLMMat2Create()
{
    GLMMat2Ref out = (GLMMat2Ref)GLM_ALLOCATE(GLMMat2);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
    return out;
}

/**
* Creates a new GLMMat2 initialized with values from an existing matrix
*
* @param a reference to matrix to clone
* @returns a new 2x2 matrix
*/
GLMMat2Ref GLMMat2Clone(GLMMat2Ref a)
{
    GLMMat2Ref out = GLMMat2Create();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    return out;

}

/**
* Copy the values from one GLMMat2 to another
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat2Copy(__OUT__ GLMMat2Ref out, GLMMat2Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
}

/**
* Set a GLMMat2 to the identity matrix
*
* @param out reference to the receiving matrix
*/
void GLMMat2Identity(__OUT__ GLMMat2Ref out)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(1.0);
}

/**
* Create a new GLMMat2 with the given values
*
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m10 Component in column 1, row 0 position (index 2)
* @param m11 Component in column 1, row 1 position (index 3)
* @returns out A new 2x2 matrix
*/
GLMMat2Ref GLMMat2FromValues(GLMRational m00, GLMRational m01, GLMRational m10, GLMRational m11)
{
    GLMMat2Ref out = GLMMat2Create();

    out[0] = m00;
    out[1] = m01;
    out[2] = m10;
    out[3] = m11;

    return out;
}

/**
* Set the components of a GLMMat2 to the given values
*
* @param out reference to the receiving matrix
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m10 Component in column 1, row 0 position (index 2)
* @param m11 Component in column 1, row 1 position (index 3)
*/
void GLMMat2Set(__OUT__ GLMMat2Ref out, GLMRational m00, GLMRational m01, GLMRational m10, GLMRational m11)
{
    out[0] = m00;
    out[1] = m01;
    out[2] = m10;
    out[3] = m11;
}


/**
* Transpose the values of a GLMMat2
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat2Transpose(__OUT__ GLMMat2Ref out, GLMMat2Ref a)
{
    // If we are transposing ourselves we can skip a few steps but have to cache some values
    if (out == a) {
        GLMRational a1 = a[1];
        out[1] = a[2];
        out[2] = a1;
    }
    else {
        out[0] = a[0];
        out[1] = a[2];
        out[2] = a[1];
        out[3] = a[3];
    }

}

/**
* Inverts a GLMMat2
*
* @param out reference to the receiving matrix, NULL if determinant of a is equal to 0
* @param a reference to the source matrix
*/
void GLMMat2Invert(__OUT__ __NULLABLE__ GLMMat2Ref out, GLMMat2Ref a)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3],

    // Calculate the determinant
    det = a0 * a3 - a2 * a1;

    if (det == GLM_RATIONAL(0.0)) {
        out = NULL;
        return;
    }

    det = GLM_RATIONAL(1.0) / det;

    out[0] = a3 * det;
    out[1] = -a1 * det;
    out[2] = -a2 * det;
    out[3] = a0 * det;
}

/**
* Calculates the adjugate of a GLMMat2
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat2Adjoint(__OUT__ GLMMat2Ref out, GLMMat2Ref a)
{
    // Caching this value is nessecary if out == a
    GLMRational a0 = a[0];
    out[0] = a[3];
    out[1] = -a[1];
    out[2] = -a[2];
    out[3] = a0;
}

/**
* Calculates the determinant of a GLMMat2
*
* @param a reference to the source matrix
* @returns determinant of a
*/
GLMRational GLMMat2Determinant(__OUT__ GLMMat2Ref a)
{
    return a[0] * a[3] - a[2] * a[1];
}

/**
* Multiplies two GLMMat2's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2Multiply(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMMat2Ref b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];

    out[0] = a0 * b0 + a2 * b1;
    out[1] = a1 * b0 + a3 * b1;
    out[2] = a0 * b2 + a2 * b3;
    out[3] = a1 * b2 + a3 * b3;
}

/**
* Rotates a GLMMat by the given angle
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat2Rotate(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMRational rad)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3],

                s = GLM_SIN(rad),
                c = GLM_COS(rad);

    out[0] = a0 *  c + a2 * s;
    out[1] = a1 *  c + a3 * s;
    out[2] = a0 * -s + a2 * c;
    out[3] = a1 * -s + a3 * c;
}

/**
* Scales the GLMMat2 by the dimensions in the given GLMvec2
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param v reference to the GLMVec2 to scale the matrix by
**/
void GLMMat2Scale(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMVec2Ref v)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3],
                v0 = v[0], v1 = v[1];

    out[0] = a0 * v0;
    out[1] = a1 * v0;
    out[2] = a2 * v1;
    out[3] = a3 * v1;
}

/**
* Sets a matrix from a given angle
*
* @param out reference to receiving operation result
* @param rad the angle to rotate the matrix by
*/
void GLMMat2FromRotation(__OUT__ GLMMat2Ref out, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad);

    out[0] = c;
    out[1] = s;
    out[2] = -s;
    out[3] = c;
}

/**
* Sets a matrix from a vector scaling
*
* @param {mat2} out mat2 receiving operation result
* @param {vec2} v Scaling vector
* @returns {mat2} out
*/
void GLMMat2FromScaling(__OUT__ GLMMat2Ref out, GLMVec2Ref v)
{
    out[0] = v[0];
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = v[1];
}

/**
* Writes string representations for GLMMat2 in out parameter.
* out must be at least 64 bytes long.
*
* @param out recieving string buffer
* @param a reference to matrix to represent as a string
*/
void GLMMat2Str(__OUT__ char *const out, GLMMat2Ref a)
{
    sprintf_s(out, 64, "GLMMat2(%f, %f, %f, %f)", a[0], a[1], a[2], a[3]);
}


/**
* Returns Frobenius norm of a GLMMat2
*
* @param a reference to the matrix to calculate Frobenius norm of
* @returns Frobenius norm
*/
GLMRational GLMMat2Frob(GLMMat2Ref a)
{
    return ( GLM_SQRT( GLM_POW(a[0], GLM_RATIONAL(2.0)) + GLM_POW(a[1], GLM_RATIONAL(2.0) ) + GLM_POW(a[2], GLM_RATIONAL(2.0) ) + GLM_POW(a[3], GLM_RATIONAL(2.0) ) ) );
}

/**
* Sets L, D and U matrices (Lower triangular, Diagonal and Upper triangular) by factorizing the input matrix
* @param L the lower triangular matrix
* @param D the diagonal matrix
* @param U the upper triangular matrix
* @param a the input matrix to factorize
*/
void GLMMat2LDU(__OUT__ GLMMat2Ref L, __OUT__ GLMMat2Ref D, __OUT__ GLMMat2Ref U, GLMMat2Ref a)
{
    L[2] = a[2] / a[0];
    U[0] = a[0];
    U[1] = a[1];
    U[3] = a[3] - L[2] * U[1];
}


/**
* Adds two GLMMat2
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2Add(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMMat2Ref b)
{
    out[0] = a[0] + b[0];
    out[1] = a[1] + b[1];
    out[2] = a[2] + b[2];
    out[3] = a[3] + b[3];
}


/**
* Subtracts matrix b from matrix a
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat2Subtract(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMMat2Ref b)
{
    out[0] = a[0] - b[0];
    out[1] = a[1] - b[1];
    out[2] = a[2] - b[2];
    out[3] = a[3] - b[3];
}


/**
* Returns whether or not the matrices have exactly the same values
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat2ExactEquals(GLMMat2Ref a, GLMMat2Ref b)
{
    return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
}

/**
* Returns whether or not the matrices have approximately the same elements in the same position.
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat2Equals(GLMMat2Ref a, GLMMat2Ref b)
{
    GLMRational a0 = a[0], a1 = a[1], a2 = a[2], a3 = a[3];
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];

    return (GLM_ABS(a0 - b0) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX( GLM_ABS(a0), GLM_ABS(b0) ) ) &&
            GLM_ABS(a1 - b1) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX( GLM_ABS(a1), GLM_ABS(b1) ) ) &&
            GLM_ABS(a2 - b2) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX( GLM_ABS(a2), GLM_ABS(b2) ) ) &&
            GLM_ABS(a3 - b3) <= GLM_EPSILON * GLM_MAX(GLM_RATIONAL(1.0), GLM_MAX( GLM_ABS(a3), GLM_ABS(b3) ) ));
}

/**
* Multiply each element of the matrix by a scalar.
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to scale
* @param b amount to scale the matrix's elements by
*/
void GLMMat2MultiplyScalar(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMRational b)
{
    out[0] = a[0] * b;
    out[1] = a[1] * b;
    out[2] = a[2] * b;
    out[3] = a[3] * b;
}

/**
* Adds two GLMMat2's after multiplying each element of the second operand by a scalar value.
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b's elements by before adding
*/
void GLMMat2MultiplyScalarAndAdd(__OUT__ GLMMat2Ref out, GLMMat2Ref a, GLMMat2Ref b, GLMRational scale)
{
    out[0] = a[0] + (b[0] * scale);
    out[1] = a[1] + (b[1] * scale);
    out[2] = a[2] + (b[2] * scale);
    out[3] = a[3] + (b[3] * scale);
}

#endif