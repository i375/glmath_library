#GL MATH#

Portable GL Math, C library, based on [gl-matrix](http://glmatrix.net/) JavaScript WebGL matrix library.

*Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV. (Original authors of gl-matrix)*

*Copyright (c) 2016 Ivane Gegia http://9221145.com/ (Porting gl-matrix to C version "GL MATH" and further extensions and additions)*

**Programming language:** C

**Library prefix:** GLM

**Library type:** Header only

**License:** MIT (see [LICENSE.md](LICENSE.md))

**Main file:** glmath.h

**Precisions:** float, double (though macro switch in ```common.h```)

**Cloning:** ```git clone https://bitbucket.org/9221145/glmath.git glmath```

[**Programmer manual**](MANUAL.md)

###Basic example###


```c++

#include "glmath/glmath.h"

int maint(void)
{
    // Allocating GLMVec2 on stack, and initializing with literals
    GLMVec2 v1 = { GLM_RATIONAL(1.0), GLM_RATIONAL(2.0) };

    // Allocating GLMVec2 on heap, heap allocated structures are automatically 
    // initialized with defaults values in case of GLMVec2 it's { 0.0,0.0 }
    GLMVec2Ref v2 = GLMVec2Create(); 

    // Setting vetor values with designated function
    GLMVec2Set(v2, GLM_RATIONAL(4.0), GLM_RATIONAL(3.0));

    //Adding v1 to v2 and writing results in v1
    GLMVec2Add(v1, v1, v2);

    //Freeing memory, allocated for v2
    GLM_FREE(v2);

    return 0;	
}

```