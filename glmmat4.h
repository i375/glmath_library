/*
Copyright (c) 2015, Brandon Jones, Colin MacKenzie IV.
Copyright (c) 2016 Ivane Gegia http://ivane.info

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/

#ifndef glmath_glmmat4_h
#define glmath_glmmat4_h

#include "common.h"

/**
* Creates a new identity GLMMat4
*
* @returns a reference to new 4x4 matrix
*/
GLMMat4Ref GLMMat4Create()
{
    GLMMat4Ref out = (GLMMat4Ref)GLM_ALLOCATE(GLMMat4);

    if (out == NULL)
    {
        return NULL;
    }

    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(0.0);
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(1.0);
    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(0.0);
    out[9] = GLM_RATIONAL(0.0);
    out[10] = GLM_RATIONAL(1.0);
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);

    return out;
}

GLMMat4Ref GLMMat4Clone(GLMMat4Ref a)
{
    GLMMat4Ref out = GLMMat4Create();

    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];

    return out;
}


/**
* Copy the values from one GLMMat4 to another
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat4Copy(__OUT__ GLMMat4Ref out, GLMMat4Ref a)
{
    out[0] = a[0];
    out[1] = a[1];
    out[2] = a[2];
    out[3] = a[3];
    out[4] = a[4];
    out[5] = a[5];
    out[6] = a[6];
    out[7] = a[7];
    out[8] = a[8];
    out[9] = a[9];
    out[10] = a[10];
    out[11] = a[11];
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
}

/**
* Create a new GLMMat4 with the given values
*
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m02 Component in column 0, row 2 position (index 2)
* @param m03 Component in column 0, row 3 position (index 3)
* @param m10 Component in column 1, row 0 position (index 4)
* @param m11 Component in column 1, row 1 position (index 5)
* @param m12 Component in column 1, row 2 position (index 6)
* @param m13 Component in column 1, row 3 position (index 7)
* @param m20 Component in column 2, row 0 position (index 8)
* @param m21 Component in column 2, row 1 position (index 9)
* @param m22 Component in column 2, row 2 position (index 10)
* @param m23 Component in column 2, row 3 position (index 11)
* @param m30 Component in column 3, row 0 position (index 12)
* @param m31 Component in column 3, row 1 position (index 13)
* @param m32 Component in column 3, row 2 position (index 14)
* @param m33 Component in column 3, row 3 position (index 15)
* @returns reference to new GLMMat4
*/
GLMMat4Ref GLMMat4FromValues(
    GLMRational m00, GLMRational m01, GLMRational m02, GLMRational m03,
    GLMRational m10, GLMRational m11, GLMRational m12, GLMRational m13,
    GLMRational m20, GLMRational m21, GLMRational m22, GLMRational m23,
    GLMRational m30, GLMRational m31, GLMRational m32, GLMRational m33)
{
    GLMMat4Ref out = GLMMat4Create();

    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;

    return out;
}

/**
* Set the components of a GLMMat4 to the given values
*
* @param out reference to the receiving matrix
* @param m00 Component in column 0, row 0 position (index 0)
* @param m01 Component in column 0, row 1 position (index 1)
* @param m02 Component in column 0, row 2 position (index 2)
* @param m03 Component in column 0, row 3 position (index 3)
* @param m10 Component in column 1, row 0 position (index 4)
* @param m11 Component in column 1, row 1 position (index 5)
* @param m12 Component in column 1, row 2 position (index 6)
* @param m13 Component in column 1, row 3 position (index 7)
* @param m20 Component in column 2, row 0 position (index 8)
* @param m21 Component in column 2, row 1 position (index 9)
* @param m22 Component in column 2, row 2 position (index 10)
* @param m23 Component in column 2, row 3 position (index 11)
* @param m30 Component in column 3, row 0 position (index 12)
* @param m31 Component in column 3, row 1 position (index 13)
* @param m32 Component in column 3, row 2 position (index 14)
* @param m33 Component in column 3, row 3 position (index 15)
*/
void GLMMat4Set(__OUT__ GLMMat4Ref out,
    GLMRational m00, GLMRational m01, GLMRational m02, GLMRational m03,
    GLMRational m10, GLMRational m11, GLMRational m12, GLMRational m13,
    GLMRational m20, GLMRational m21, GLMRational m22, GLMRational m23,
    GLMRational m30, GLMRational m31, GLMRational m32, GLMRational m33)
{
    out[0] = m00;
    out[1] = m01;
    out[2] = m02;
    out[3] = m03;
    out[4] = m10;
    out[5] = m11;
    out[6] = m12;
    out[7] = m13;
    out[8] = m20;
    out[9] = m21;
    out[10] = m22;
    out[11] = m23;
    out[12] = m30;
    out[13] = m31;
    out[14] = m32;
    out[15] = m33;
}


/**
* Set a GLMMat4 to the identity matrix
*
* @param out reference the receiving matrix
*/
void GLMMat4Identity(__IN_OUT__ GLMMat4Ref out)
{
    out[0] = GLM_RATIONAL(1.0);
    out[1] = GLM_RATIONAL(0.0);
    out[2] = GLM_RATIONAL(0.0);
    out[3] = GLM_RATIONAL(0.0);
    out[4] = GLM_RATIONAL(0.0);
    out[5] = GLM_RATIONAL(1.0);
    out[6] = GLM_RATIONAL(0.0);
    out[7] = GLM_RATIONAL(0.0);
    out[8] = GLM_RATIONAL(0.0);
    out[9] = GLM_RATIONAL(0.0);
    out[10] = GLM_RATIONAL(1.0);
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Transpose the values of a GLMMat4
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat4Transpose(__OUT__ GLMMat4Ref out, GLMMat4Ref a)
{
    // Chaching values if transposing our selves
    if (out == a) 
    {
        GLMRational a01 = a[1], a02 = a[2], a03 = a[3],
                    a12 = a[6], a13 = a[7],
                    a23 = a[11];

        out[1]  = a[4];
        out[2]  = a[8];
        out[3]  = a[12];
        out[4]  = a01;
        out[6]  = a[9];
        out[7]  = a[13];
        out[8]  = a02;
        out[9]  = a12;
        out[11] = a[14];
        out[12] = a03;
        out[13] = a13;
        out[14] = a23;
    }
    else 
    {
        out[0]  = a[0];
        out[1]  = a[4];
        out[2]  = a[8];
        out[3]  = a[12];
        out[4]  = a[1];
        out[5]  = a[5];
        out[6]  = a[9];
        out[7]  = a[13];
        out[8]  = a[2];
        out[9]  = a[6];
        out[10] = a[10];
        out[11] = a[14];
        out[12] = a[3];
        out[13] = a[7];
        out[14] = a[11];
        out[15] = a[15];
    }
}

/**
* Transpose the values of a GLMMat4
*
* It's faster than calling:
*
* GLMMat4Transpose(dest, dest);
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat4TransposeSelf(__IN_OUT__ GLMMat4Ref out)
{
    GLMRational a01 = out[1], a02 = out[2], a03 = out[3],
                a12 = out[6], a13 = out[7],
                a23 = out[11];

    out[1] = out[4];
    out[2] = out[8];
    out[3] = out[12];
    out[4] = a01;
    out[6] = out[9];
    out[7] = out[13];
    out[8] = a02;
    out[9] = a12;
    out[11] = out[14];
    out[12] = a03;
    out[13] = a13;
    out[14] = a23;
}

/**
* Inverts a GLMMat4
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat4Invert(__OUT__ __NULLABLE__ GLMMat4Ref out, GLMMat4Ref a)
{
    GLMRational a00 = a[0], a01 = a[1], a02 = a[2], a03 = a[3],
                a10 = a[4], a11 = a[5], a12 = a[6], a13 = a[7],
                a20 = a[8], a21 = a[9], a22 = a[10], a23 = a[11],
                a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

        b00 = a00 * a11 - a01 * a10,
        b01 = a00 * a12 - a02 * a10,
        b02 = a00 * a13 - a03 * a10,
        b03 = a01 * a12 - a02 * a11,
        b04 = a01 * a13 - a03 * a11,
        b05 = a02 * a13 - a03 * a12,
        b06 = a20 * a31 - a21 * a30,
        b07 = a20 * a32 - a22 * a30,
        b08 = a20 * a33 - a23 * a30,
        b09 = a21 * a32 - a22 * a31,
        b10 = a21 * a33 - a23 * a31,
        b11 = a22 * a33 - a23 * a32,

        // Computing determinant
        det = b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;

    if (det == GLM_RATIONAL(0.0))
    {
        out = NULL;
        return;
    }

    det = GLM_RATIONAL(1.0) / det;

    out[0] = (a11 * b11 - a12 * b10 + a13 * b09) * det;
    out[1] = (a02 * b10 - a01 * b11 - a03 * b09) * det;
    out[2] = (a31 * b05 - a32 * b04 + a33 * b03) * det;
    out[3] = (a22 * b04 - a21 * b05 - a23 * b03) * det;
    out[4] = (a12 * b08 - a10 * b11 - a13 * b07) * det;
    out[5] = (a00 * b11 - a02 * b08 + a03 * b07) * det;
    out[6] = (a32 * b02 - a30 * b05 - a33 * b01) * det;
    out[7] = (a20 * b05 - a22 * b02 + a23 * b01) * det;
    out[8] = (a10 * b10 - a11 * b08 + a13 * b06) * det;
    out[9] = (a01 * b08 - a00 * b10 - a03 * b06) * det;
    out[10] = (a30 * b04 - a31 * b02 + a33 * b00) * det;
    out[11] = (a21 * b02 - a20 * b04 - a23 * b00) * det;
    out[12] = (a11 * b07 - a10 * b09 - a12 * b06) * det;
    out[13] = (a00 * b09 - a01 * b07 + a02 * b06) * det;
    out[14] = (a31 * b01 - a30 * b03 - a32 * b00) * det;
    out[15] = (a20 * b03 - a21 * b01 + a22 * b00) * det;
}

/**
* Computes the adjugate of a GLMMat4
*
* @param out reference to the receiving matrix
* @param a reference to the source matrix
*/
void GLMMat4Adjoint(__OUT__ GLMMat4Ref out, GLMMat4Ref a)
{
    GLMRational a00 = a[0],  a01 = a[1],  a02 = a[2],  a03 = a[3],
                a10 = a[4],  a11 = a[5],  a12 = a[6],  a13 = a[7],
                a20 = a[8],  a21 = a[9],  a22 = a[10], a23 = a[11],
                a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    out[0] =   (a11 * (a22 * a33 - a23 * a32) - a21 * (a12 * a33 - a13 * a32) + a31 * (a12 * a23 - a13 * a22));
    out[1] =  -(a01 * (a22 * a33 - a23 * a32) - a21 * (a02 * a33 - a03 * a32) + a31 * (a02 * a23 - a03 * a22));
    out[2] =   (a01 * (a12 * a33 - a13 * a32) - a11 * (a02 * a33 - a03 * a32) + a31 * (a02 * a13 - a03 * a12));
    out[3] =  -(a01 * (a12 * a23 - a13 * a22) - a11 * (a02 * a23 - a03 * a22) + a21 * (a02 * a13 - a03 * a12));
    out[4] =  -(a10 * (a22 * a33 - a23 * a32) - a20 * (a12 * a33 - a13 * a32) + a30 * (a12 * a23 - a13 * a22));
    out[5] =   (a00 * (a22 * a33 - a23 * a32) - a20 * (a02 * a33 - a03 * a32) + a30 * (a02 * a23 - a03 * a22));
    out[6] =  -(a00 * (a12 * a33 - a13 * a32) - a10 * (a02 * a33 - a03 * a32) + a30 * (a02 * a13 - a03 * a12));
    out[7] =   (a00 * (a12 * a23 - a13 * a22) - a10 * (a02 * a23 - a03 * a22) + a20 * (a02 * a13 - a03 * a12));
    out[8] =   (a10 * (a21 * a33 - a23 * a31) - a20 * (a11 * a33 - a13 * a31) + a30 * (a11 * a23 - a13 * a21));
    out[9] =  -(a00 * (a21 * a33 - a23 * a31) - a20 * (a01 * a33 - a03 * a31) + a30 * (a01 * a23 - a03 * a21));
    out[10] =  (a00 * (a11 * a33 - a13 * a31) - a10 * (a01 * a33 - a03 * a31) + a30 * (a01 * a13 - a03 * a11));
    out[11] = -(a00 * (a11 * a23 - a13 * a21) - a10 * (a01 * a23 - a03 * a21) + a20 * (a01 * a13 - a03 * a11));
    out[12] = -(a10 * (a21 * a32 - a22 * a31) - a20 * (a11 * a32 - a12 * a31) + a30 * (a11 * a22 - a12 * a21));
    out[13] =  (a00 * (a21 * a32 - a22 * a31) - a20 * (a01 * a32 - a02 * a31) + a30 * (a01 * a22 - a02 * a21));
    out[14] = -(a00 * (a11 * a32 - a12 * a31) - a10 * (a01 * a32 - a02 * a31) + a30 * (a01 * a12 - a02 * a11));
    out[15] =  (a00 * (a11 * a22 - a12 * a21) - a10 * (a01 * a22 - a02 * a21) + a20 * (a01 * a12 - a02 * a11));
}


/**
* Computes the determinant of a GLMMat4
*
* @param a reference to the source matrix
* @returns determinant of a
*/
GLMRational GLMMat4Determinant(__OUT__ GLMMat4Ref a)
{
    GLMRational a00 = a[0],  a01 = a[1],  a02 = a[2],  a03 = a[3],
                a10 = a[4],  a11 = a[5],  a12 = a[6],  a13 = a[7],
                a20 = a[8],  a21 = a[9],  a22 = a[10], a23 = a[11],
                a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15],

                b00 = a00 * a11 - a01 * a10,
                b01 = a00 * a12 - a02 * a10,
                b02 = a00 * a13 - a03 * a10,
                b03 = a01 * a12 - a02 * a11,
                b04 = a01 * a13 - a03 * a11,
                b05 = a02 * a13 - a03 * a12,
                b06 = a20 * a31 - a21 * a30,
                b07 = a20 * a32 - a22 * a30,
                b08 = a20 * a33 - a23 * a30,
                b09 = a21 * a32 - a22 * a31,
                b10 = a21 * a33 - a23 * a31,
                b11 = a22 * a33 - a23 * a32;

    return b00 * b11 - b01 * b10 + b02 * b09 + b03 * b08 - b04 * b07 + b05 * b06;
}

/**
* Multiplies two GLMMat4's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat4Multiply(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMMat4Ref b)
{
    GLMRational a00 = a[0],  a01 = a[1],  a02 = a[2],  a03 = a[3],
                a10 = a[4],  a11 = a[5],  a12 = a[6],  a13 = a[7],
                a20 = a[8],  a21 = a[9],  a22 = a[10], a23 = a[11],
                a30 = a[12], a31 = a[13], a32 = a[14], a33 = a[15];

    // Caching only the current line of the second matrix
    GLMRational b0 = b[0], b1 = b[1], b2 = b[2], b3 = b[3];
    out[0] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[1] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[2] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[3] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[4]; b1 = b[5]; b2 = b[6]; b3 = b[7];
    out[4] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[5] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[6] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[7] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[8]; b1 = b[9]; b2 = b[10]; b3 = b[11];
    out[8] =  b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[9] =  b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[10] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[11] = b0*a03 + b1*a13 + b2*a23 + b3*a33;

    b0 = b[12]; b1 = b[13]; b2 = b[14]; b3 = b[15];
    out[12] = b0*a00 + b1*a10 + b2*a20 + b3*a30;
    out[13] = b0*a01 + b1*a11 + b2*a21 + b3*a31;
    out[14] = b0*a02 + b1*a12 + b2*a22 + b3*a32;
    out[15] = b0*a03 + b1*a13 + b2*a23 + b3*a33;
}


/**
* Translate a GLMMat4 by the given vector
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to translate
* @param v reference to vector to translate by
*/
void GLMMat4Translate(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMVec3Ref v)
{
    GLMRational x = v[0], y = v[1], z = v[2],
                a00, a01, a02, a03,
                a10, a11, a12, a13,
                a20, a21, a22, a23;

    if (a == out) 
    {
        out[12] = a[0] * x + a[4] * y + a[8] * z + a[12];
        out[13] = a[1] * x + a[5] * y + a[9] * z + a[13];
        out[14] = a[2] * x + a[6] * y + a[10] * z + a[14];
        out[15] = a[3] * x + a[7] * y + a[11] * z + a[15];
    }
    else 
    {
        a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
        a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
        a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];

        out[0] = a00; out[1] = a01; out[2] = a02; out[3] = a03;
        out[4] = a10; out[5] = a11; out[6] = a12; out[7] = a13;
        out[8] = a20; out[9] = a21; out[10] = a22; out[11] = a23;

        out[12] = a00 * x + a10 * y + a20 * z + a[12];
        out[13] = a01 * x + a11 * y + a21 * z + a[13];
        out[14] = a02 * x + a12 * y + a22 * z + a[14];
        out[15] = a03 * x + a13 * y + a23 * z + a[15];
    }
}

/**
* Scales the GLMMat4 by the dimensions in the given GLMVec3
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to scale
* @param v reference to the GLMVec3 to scale the matrix by
**/
void GLMMat4Scale(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMVec3Ref v)
{
    GLMRational x = v[0], y = v[1], z = v[2];

    out[0]  = a[0] * x;
    out[1]  = a[1] * x;
    out[2]  = a[2] * x;
    out[3]  = a[3] * x;
    out[4]  = a[4] * y;
    out[5]  = a[5] * y;
    out[6]  = a[6] * y;
    out[7]  = a[7] * y;
    out[8]  = a[8] * z;
    out[9]  = a[9] * z;
    out[10] = a[10] * z;
    out[11] = a[11] * z;
    out[12] = a[12];
    out[13] = a[13];
    out[14] = a[14];
    out[15] = a[15];
}

/**
* Rotates a GLMMat4 by the given angle around the given axis
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
* @param axis reference to the axis to rotate around
*/
void GLMMat4Rotate(__OUT__ __NULLABLE__ GLMMat4Ref out, GLMMat4Ref a, GLMRational rad, GLMVec3Ref axis)
{
    GLMRational x = axis[0], y = axis[1], z = axis[2],
                len = GLM_SQRT(x * x + y * y + z * z),
                s, c, t,
                a00, a01, a02, a03,
                a10, a11, a12, a13,
                a20, a21, a22, a23,
                b00, b01, b02,
                b10, b11, b12,
                b20, b21, b22;

    //If axis length is less than GLM_EPSILON don't do a thing
    if (GLM_ABS(len) < GLM_EPSILON) 
    { 
        out = NULL;
        return; 
    }

    len = GLM_RATIONAL(1.0) / len;
    x *= len;
    y *= len;
    z *= len;

    s = GLM_SIN(rad);
    c = GLM_COS(rad);
    t = GLM_RATIONAL(1.0) - c;

    a00 = a[0]; a01 = a[1]; a02 = a[2]; a03 = a[3];
    a10 = a[4]; a11 = a[5]; a12 = a[6]; a13 = a[7];
    a20 = a[8]; a21 = a[9]; a22 = a[10]; a23 = a[11];

    // Construct the elements of the rotation matrix
    b00 = x * x * t + c; b01 = y * x * t + z * s; b02 = z * x * t - y * s;
    b10 = x * y * t - z * s; b11 = y * y * t + c; b12 = z * y * t + x * s;
    b20 = x * z * t + y * s; b21 = y * z * t - x * s; b22 = z * z * t + c;

    // Perform rotation-specific matrix multiplication
    out[0]  = a00 * b00 + a10 * b01 + a20 * b02;
    out[1]  = a01 * b00 + a11 * b01 + a21 * b02;
    out[2]  = a02 * b00 + a12 * b01 + a22 * b02;
    out[3]  = a03 * b00 + a13 * b01 + a23 * b02;
    out[4]  = a00 * b10 + a10 * b11 + a20 * b12;
    out[5]  = a01 * b10 + a11 * b11 + a21 * b12;
    out[6]  = a02 * b10 + a12 * b11 + a22 * b12;
    out[7]  = a03 * b10 + a13 * b11 + a23 * b12;
    out[8]  = a00 * b20 + a10 * b21 + a20 * b22;
    out[9]  = a01 * b20 + a11 * b21 + a21 * b22;
    out[10] = a02 * b20 + a12 * b21 + a22 * b22;
    out[11] = a03 * b20 + a13 * b21 + a23 * b22;

    // If the source and destination differ, copy the unchanged last row
    if (a != out) 
    { 
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }
}

/**
* Rotates a matrix by the given angle around the X axis
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat4RotateX(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad),
                a10 = a[4],
                a11 = a[5],
                a12 = a[6],
                a13 = a[7],
                a20 = a[8],
                a21 = a[9],
                a22 = a[10],
                a23 = a[11];

    // If the source and destination differ, copy the unchanged rows
    if (a != out) 
    { 
        out[0]  = a[0];
        out[1]  = a[1];
        out[2]  = a[2];
        out[3]  = a[3];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[4]  = a10 * c + a20 * s;
    out[5]  = a11 * c + a21 * s;
    out[6]  = a12 * c + a22 * s;
    out[7]  = a13 * c + a23 * s;
    out[8]  = a20 * c - a10 * s;
    out[9]  = a21 * c - a11 * s;
    out[10] = a22 * c - a12 * s;
    out[11] = a23 * c - a13 * s;
}

/**
* Rotates a matrix by the given angle around the Y axis
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat4RotateY(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMRational rad)
{
    GLMRational s   = GLM_SIN(rad),
                c   = GLM_COS(rad),
                a00 = a[0],
                a01 = a[1],
                a02 = a[2],
                a03 = a[3],
                a20 = a[8],
                a21 = a[9],
                a22 = a[10],
                a23 = a[11];

    // If the source and destination differ, copy the unchanged rows
    if (a != out) 
    { 
        out[4]  = a[4];
        out[5]  = a[5];
        out[6]  = a[6];
        out[7]  = a[7];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0]  = a00 * c - a20 * s;
    out[1]  = a01 * c - a21 * s;
    out[2]  = a02 * c - a22 * s;
    out[3]  = a03 * c - a23 * s;
    out[8]  = a00 * s + a20 * c;
    out[9]  = a01 * s + a21 * c;
    out[10] = a02 * s + a22 * c;
    out[11] = a03 * s + a23 * c;
}

/**
* Rotates a matrix by the given angle around the Z axis
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to rotate
* @param rad the angle to rotate the matrix by
*/
void GLMMat4RotateZ(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad),
                a00 = a[0],
                a01 = a[1],
                a02 = a[2],
                a03 = a[3],
                a10 = a[4],
                a11 = a[5],
                a12 = a[6],
                a13 = a[7];

    // If the source and destination differ, copy the unchanged last row
    if (a != out) 
    { 
        out[8]  = a[8];
        out[9]  = a[9];
        out[10] = a[10];
        out[11] = a[11];
        out[12] = a[12];
        out[13] = a[13];
        out[14] = a[14];
        out[15] = a[15];
    }

    // Perform axis-specific matrix multiplication
    out[0] = a00 * c + a10 * s;
    out[1] = a01 * c + a11 * s;
    out[2] = a02 * c + a12 * s;
    out[3] = a03 * c + a13 * s;
    out[4] = a10 * c - a00 * s;
    out[5] = a11 * c - a01 * s;
    out[6] = a12 * c - a02 * s;
    out[7] = a13 * c - a03 * s;
}

/**
* Computes a matrix from a vector translation
*
* @param out reference to receiving operation result
* @param v reference to translation vector
*/
void GLMMat4FromTranslation(__IN_OUT__ GLMMat4Ref out, GLMVec3Ref v)
{
    out[0]  = GLM_RATIONAL(1.0);
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = GLM_RATIONAL(1.0);
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = GLM_RATIONAL(0.0);
    out[9]  = GLM_RATIONAL(0.0);
    out[10] = GLM_RATIONAL(1.0);
    out[11] = GLM_RATIONAL(0.0);
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a matrix from a vector scaling
*
* @param out reference to receiving operation result
* @param v reference to caling vector
*/
void GLMMat4FromScaling(__IN_OUT__ GLMMat4Ref out, GLMVec3Ref v)
{
    out[0]  = v[0];
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = v[1];
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = GLM_RATIONAL(0.0);
    out[9]  = GLM_RATIONAL(0.0);
    out[10] = v[2];
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}


/**
* Computes a matrix from a given angle around a given axis
*
* @param out reference to receiving matrix operation result
* @param rad the angle to rotate the matrix by
* @param axis reference to the axis vector to rotate around
*/
void GLMMat4FromRotation(__IN_OUT__ __NULLABLE__ GLMMat4Ref out, GLMRational rad, GLMVec3Ref axis)
{
    GLMRational x = axis[0], y = axis[1], z = axis[2],
                len = GLM_SQRT(x * x + y * y + z * z),
                s, c, t;

    // If lenth of axis vector is less than GLM_EPSILON, don't do a thing
    if (GLM_ABS(len) < GLM_EPSILON) 
    { 
        out = NULL;
        return; 
    }

    len = GLM_RATIONAL(1.0) / len;
    x *= len;
    y *= len;
    z *= len;

    s = GLM_SIN(rad);
    c = GLM_COS(rad);
    t = GLM_RATIONAL(1.0) - c;

    // Perform rotation-specific matrix multiplication
    out[0]  = x * x * t + c;
    out[1]  = y * x * t + z * s;
    out[2]  = z * x * t - y * s;
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = x * y * t - z * s;
    out[5]  = y * y * t + c;
    out[6]  = z * y * t + x * s;
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = x * z * t + y * s;
    out[9]  = y * z * t - x * s;
    out[10] = z * z * t + c;
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a matrix from the given angle around the X axis
*
* @param out reference to receiving operation result
* @param rad the angle to rotate the matrix by
*/
void GLMMat4FromXRotation(__IN_OUT__ GLMMat4Ref out, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad);

    // Axis-specific matrix multiplication
    out[0]  = GLM_RATIONAL(1.0);
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = c;
    out[6]  = s;
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = GLM_RATIONAL(0.0);
    out[9]  = -s;
    out[10] = c;
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a matrix from the given angle around the Y axis
*
* @param out reference to receiving operation result
* @param rad the angle to rotate the matrix by
*/
void GLMMat4FromYRotation(__IN_OUT__ GLMMat4Ref out, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad);

    // Axis-specific matrix multiplication
    out[0]  = c;
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = -s;
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = GLM_RATIONAL(1.0);
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = s;
    out[9]  = GLM_RATIONAL(0.0);
    out[10] = c;
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}


/**
* Computes a matrix from the given angle around the Z axis
*
* @param out reference to receiving operation result
* @param rad the angle to rotate the matrix by
*/
void GLMMat4FromZRotation(__IN_OUT__ GLMMat4Ref out, GLMRational rad)
{
    GLMRational s = GLM_SIN(rad),
                c = GLM_COS(rad);

    // Axis-specific matrix multiplication
    out[0]  = c;
    out[1]  = s;
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = -s;
    out[5]  = c;
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = GLM_RATIONAL(0.0);
    out[9]  = GLM_RATIONAL(0.0);
    out[10] = GLM_RATIONAL(1.0);
    out[11] = GLM_RATIONAL(0.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}


/**
* Computes a matrix from a quaternion rotation and vector translation
*
* @param out reference to receiving matrix
* @param q reference to rotation quaternion
* @param v reference to translation vector
*/
void GLMMat4FromRotationTranslation(__IN_OUT__ GLMMat4Ref out, GLMQuatRef q, GLMVec3Ref v)
{
    // Quaternion math
    GLMRational x = q[0], y = q[1], z = q[2], w = q[3],
                x2 = x + x,
                y2 = y + y,
                z2 = z + z,

                xx = x * x2,
                xy = x * y2,
                xz = x * z2,
                yy = y * y2,
                yz = y * z2,
                zz = z * z2,
                wx = w * x2,
                wy = w * y2,
                wz = w * z2;

    out[0]  = GLM_RATIONAL(1.0) - (yy + zz);
    out[1]  = xy + wz;
    out[2]  = xz - wy;
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = xy - wz;
    out[5]  = GLM_RATIONAL(1.0) - (xx + zz);
    out[6]  = yz + wx;
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = xz + wy;
    out[9]  = yz - wx;
    out[10] = GLM_RATIONAL(1.0) - (xx + yy);
    out[11] = GLM_RATIONAL(0.0);
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a matrix from a quaternion rotation, vector translation and vector scale
* 
* @param out reference to receiving operation result
* @param q reference to rotation quaternion
* @param v reference to translation vector
* @param s reference to scaling vector
*/
void GLMMat4FromRotationTranslationScale(__IN_OUT__ GLMMat4Ref out, GLMQuatRef q, GLMVec3Ref v, GLMVec3Ref s)
{
    // Quaternion math
    GLMRational x = q[0], y = q[1], z = q[2], w = q[3],
                x2 = x + x,
                y2 = y + y,
                z2 = z + z,

                xx = x * x2,
                xy = x * y2,
                xz = x * z2,
                yy = y * y2,
                yz = y * z2,
                zz = z * z2,
                wx = w * x2,
                wy = w * y2,
                wz = w * z2,
                sx = s[0],
                sy = s[1],
                sz = s[2];

    out[0]  = (GLM_RATIONAL(1.0) - (yy + zz)) * sx;
    out[1]  = (xy + wz) * sx;
    out[2]  = (xz - wy) * sx;
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = (xy - wz) * sy;
    out[5]  = (GLM_RATIONAL(1.0) - (xx + zz)) * sy;
    out[6]  = (yz + wx) * sy;
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = (xz + wy) * sz;
    out[9]  = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = GLM_RATIONAL(0.0);
    out[12] = v[0];
    out[13] = v[1];
    out[14] = v[2];
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a matrix from a quaternion rotation, vector translation and vector scale, rotating and scaling around the given origin
*
* @param out reference to receiving operation result
* @param q reference to rotation quaternion
* @param v reference to translation vector
* @param s reference to scaling vector
* @param o reference to the origin vector around which to scale and rotate
*/
void GLMMat4FromRotationTranslationScaleOrigin(__IN_OUT__ GLMMat4Ref out, GLMQuatRef q, GLMVec3Ref v, GLMVec3Ref s, GLMVec3Ref o)
{
    // Quaternion math
    GLMRational x = q[0], y = q[1], z = q[2], w = q[3],
                x2 = x + x,
                y2 = y + y,
                z2 = z + z,

                xx = x * x2,
                xy = x * y2,
                xz = x * z2,
                yy = y * y2,
                yz = y * z2,
                zz = z * z2,
                wx = w * x2,
                wy = w * y2,
                wz = w * z2,

                sx = s[0],
                sy = s[1],
                sz = s[2],

                ox = o[0],
                oy = o[1],
                oz = o[2];

    out[0]  = (GLM_RATIONAL(1.0) - (yy + zz)) * sx;
    out[1]  = (xy + wz) * sx;
    out[2]  = (xz - wy) * sx;
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = (xy - wz) * sy;
    out[5]  = (1 - (xx + zz)) * sy;
    out[6]  = (yz + wx) * sy;
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = (xz + wy) * sz;
    out[9]  = (yz - wx) * sz;
    out[10] = (1 - (xx + yy)) * sz;
    out[11] = GLM_RATIONAL(0.0);
    out[12] = v[0] + ox - (out[0] * ox + out[4] * oy + out[8] * oz);
    out[13] = v[1] + oy - (out[1] * ox + out[5] * oy + out[9] * oz);
    out[14] = v[2] + oz - (out[2] * ox + out[6] * oy + out[10] * oz);
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a 4x4 matrix from the given quaternion
*
* @param out reference to receiving operation result
* @param q reference to quaternion to create matrix from
*/
void GLMMat4FromQuat(__IN_OUT__ GLMMat4Ref out, GLMQuatRef q)
{
    GLMRational x = q[0], y = q[1], z = q[2], w = q[3],
                x2 = x + x,
                y2 = y + y,
                z2 = z + z,

                xx = x * x2,
                yx = y * x2,
                yy = y * y2,
                zx = z * x2,
                zy = z * y2,
                zz = z * z2,
                wx = w * x2,
                wy = w * y2,
                wz = w * z2;

    out[0]  = GLM_RATIONAL(1.0) - yy - zz;
    out[1]  = yx + wz;
    out[2]  = zx - wy;
    out[3]  = GLM_RATIONAL(0.0);

    out[4]  = yx - wz;
    out[5]  = GLM_RATIONAL(1.0) - xx - zz;
    out[6]  = zy + wx;
    out[7]  = GLM_RATIONAL(0.0);

    out[8]  = zx + wy;
    out[9]  = zy - wx;
    out[10] = GLM_RATIONAL(1.0) - xx - yy;
    out[11] = GLM_RATIONAL(0.0);

    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = GLM_RATIONAL(0.0);
    out[15] = GLM_RATIONAL(1.0);
}

/**
* Computes a frustum matrix with the given bounds
*
* @param out reference to GLMMat4 frustum matrix will be written into
* @param left Left bound of the frustum
* @param right Right bound of the frustum
* @param bottom Bottom bound of the frustum
* @param top Top bound of the frustum
* @param _near _near bound of the frustum
* @param _far _far bound of the frustum
*/
void GLMMat4Frustum(__IN_OUT__ GLMMat4Ref out, GLMRational left, GLMRational right, GLMRational bottom, GLMRational top, GLMRational _near, GLMRational _far)
{
    GLMRational rl = GLM_RATIONAL(1.0) / (right - left),
                tb = GLM_RATIONAL(1.0) / (top - bottom),
                nf = GLM_RATIONAL(1.0) / (_near - _far);

    out[0]  = (_near * GLM_RATIONAL(2.0)) * rl;
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = (_near * GLM_RATIONAL(2.0)) * tb;
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = (right + left) * rl;
    out[9]  = (top + bottom) * tb;
    out[10] = (_far + _near) * nf;
    out[11] = -GLM_RATIONAL(1.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = (_far * _near * GLM_RATIONAL(2.0)) * nf;
    out[15] = GLM_RATIONAL(0.0);
}

/**
* Computes a perspective projection matrix with the given bounds
*
* @param out reference to frustum matrix will be written into
* @param fovy Vertical field of view in radians
* @param aspect Aspect ratio. typically viewport width/height
* @param _near _near bound of the frustum
* @param _far _far bound of the frustum
*/
void GLMMat4Perspective(__IN_OUT__ GLMMat4Ref out, GLMRational fovY, GLMRational aspectRatio, GLMRational _near, GLMRational _far)
{
    GLMRational f = GLM_RATIONAL(1.0) / GLM_TAN(fovY / GLM_RATIONAL(2.0)),
                nf = GLM_RATIONAL(1.0) / (_near - _far);

    out[0]  = f / aspectRatio;
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = f;
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = GLM_RATIONAL(0.0);
    out[9]  = GLM_RATIONAL(0.0);
    out[10] = (_far + _near) * nf;
    out[11] = -GLM_RATIONAL(1.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = (GLM_RATIONAL(2.0) * _far * _near) * nf;
    out[15] = GLM_RATIONAL(0.0);
}



/**
* Computes a perspective projection matrix with the given field of view.
* This is primarily useful for generating projection matrices to be used
* with the still experiemental VR API.
*
* @param out mat4 frustum matrix will be written into
* @param fov reference to four parameter field of view definition
* @param _near _near bound of the frustum
* @param _far _far bound of the frustum
*/
void GLMMat4PerspectiveFromFieldOfView(__IN_OUT__ GLMMat4Ref out, GLMFOV4Ref fov, GLMRational _near, GLMRational _far)
{
    GLMRational upTan = GLM_TAN(fov->upDegrees * GLM_PI / GLM_RATIONAL(180.0)),
                downTan = GLM_TAN(fov->downDegrees * GLM_PI / GLM_RATIONAL(180.0)),
                leftTan = GLM_TAN(fov->leftDegrees * GLM_PI / GLM_RATIONAL(180.0)),
                rightTan = GLM_TAN(fov->rightDegrees * GLM_PI / GLM_RATIONAL(180.0)),
                xScale = GLM_RATIONAL(2.0) / (leftTan + rightTan),
                yScale = GLM_RATIONAL(2.0) / (upTan + downTan);

    out[0]  = xScale;
    out[1]  = GLM_RATIONAL(0.0);
    out[2]  = GLM_RATIONAL(0.0);
    out[3]  = GLM_RATIONAL(0.0);
    out[4]  = GLM_RATIONAL(0.0);
    out[5]  = yScale;
    out[6]  = GLM_RATIONAL(0.0);
    out[7]  = GLM_RATIONAL(0.0);
    out[8]  = -((leftTan - rightTan) * xScale * 0.5F);
    out[9]  = ((upTan - downTan) * yScale * 0.5F);
    out[10] = _far / (_near - _far);
    out[11] = -GLM_RATIONAL(1.0);
    out[12] = GLM_RATIONAL(0.0);
    out[13] = GLM_RATIONAL(0.0);
    out[14] = (_far * _near) / (_near - _far);
    out[15] = GLM_RATIONAL(0.0);
}

/**
* Computes a orthogonal projection matrix with the given bounds
*
* @param out reference to GLMMat4 frustum matrix will be written into
* @param left Left bound of the frustum
* @param right Right bound of the frustum
* @param bottom Bottom bound of the frustum
* @param top Top bound of the frustum
* @param _near _near bound of the frustum
* @param _far _far bound of the frustum
*/
void GLMMat4Ortho(__IN_OUT__ GLMMat4Ref out, GLMRational left, GLMRational right, GLMRational bottom, GLMRational top, GLMRational _near, GLMRational _far)
{
    GLMRational lr = GLM_RATIONAL(1.0) / (left - right),
                bt = GLM_RATIONAL(1.0) / (bottom - top),
                nf = GLM_RATIONAL(1.0) / (_near - _far);

    out[0]  = -GLM_RATIONAL(2.0) * lr;
    out[1]  =  GLM_RATIONAL(0.0);
    out[2]  =  GLM_RATIONAL(0.0);
    out[3]  =  GLM_RATIONAL(0.0);
    out[4]  =  GLM_RATIONAL(0.0);
    out[5]  = -GLM_RATIONAL(2.0) * bt;
    out[6]  =  GLM_RATIONAL(0.0);
    out[7]  =  GLM_RATIONAL(0.0);
    out[8]  =  GLM_RATIONAL(0.0);
    out[9]  =  GLM_RATIONAL(0.0);
    out[10] =  GLM_RATIONAL(2.0) * nf;
    out[11] =  GLM_RATIONAL(0.0);
    out[12] =  (left + right) * lr;
    out[13] =  (top + bottom) * bt;
    out[14] =  (_far + _near) * nf;
    out[15] =  GLM_RATIONAL(1.0);
}

/**
* Computes a look-at matrix with the given eye position, focal point, and up axis
*
* @param out reference to frustum matrix will be written into
* @param eye reference to position of the viewer 
* @param center point the viewer is looking at 
* @param up reference to UP vector
*/
void GLMMat4LookAt(__IN_OUT__ GLMMat4Ref out, GLMVec3Ref eye, GLMVec3Ref center, GLMVec3Ref up)
{
    GLMRational x0, x1, x2, y0, y1, y2, z0, z1, z2, len,
                eyex = eye[0],
                eyey = eye[1],
                eyez = eye[2],
                upx = up[0],
                upy = up[1],
                upz = up[2],
                centerx = center[0],
                centery = center[1],
                centerz = center[2];

    if (GLM_ABS(eyex - centerx) < GLM_EPSILON &&
        GLM_ABS(eyey - centery) < GLM_EPSILON &&
        GLM_ABS(eyez - centerz) < GLM_EPSILON) 
    {

        GLMMat4Identity(out);
        return;

    }

    z0 = eyex - centerx;
    z1 = eyey - centery;
    z2 = eyez - centerz;

    len = GLM_RATIONAL(1.0) / GLM_SQRT(z0 * z0 + z1 * z1 + z2 * z2);
    z0 *= len;
    z1 *= len;
    z2 *= len;

    x0 = upy * z2 - upz * z1;
    x1 = upz * z0 - upx * z2;
    x2 = upx * z1 - upy * z0;
    len = GLM_SQRT(x0 * x0 + x1 * x1 + x2 * x2);

    if (len == GLM_RATIONAL(0.0)) 
    {
        x0 = GLM_RATIONAL(0.0);
        x1 = GLM_RATIONAL(0.0);
        x2 = GLM_RATIONAL(0.0);
    }
    else 
    {
        len = GLM_RATIONAL(1.0) / len;
        x0 *= len;
        x1 *= len;
        x2 *= len;
    }

    y0 = z1 * x2 - z2 * x1;
    y1 = z2 * x0 - z0 * x2;
    y2 = z0 * x1 - z1 * x0;

    len = GLM_SQRT(y0 * y0 + y1 * y1 + y2 * y2);

    if (len == 0) 
    {
        y0 = GLM_RATIONAL(0.0);
        y1 = GLM_RATIONAL(0.0);
        y2 = GLM_RATIONAL(0.0);
    }
    else 
    {
        len = GLM_RATIONAL(1.0) / len;
        y0 *= len;
        y1 *= len;
        y2 *= len;
    }

    out[0]  =  x0;
    out[1]  =  y0;
    out[2]  =  z0;
    out[3]  =  GLM_RATIONAL(0.0);
    out[4]  =  x1;
    out[5]  =  y1;
    out[6]  =  z1;
    out[7]  =  GLM_RATIONAL(0.0);
    out[8]  =  x2;
    out[9]  =  y2;
    out[10] =  z2;
    out[11] =  GLM_RATIONAL(0.0);
    out[12] = -(x0 * eyex + x1 * eyey + x2 * eyez);
    out[13] = -(y0 * eyex + y1 * eyey + y2 * eyez);
    out[14] = -(z0 * eyex + z1 * eyey + z2 * eyez);
    out[15] =  GLM_RATIONAL(1.0);
}

/**
* Writes string representations for GLMMat4 in out parameter.
* out must be at least 512 bytes long.
*
* @param out recieving string buffer
* @param a reference to matrix to represent as a string
*/
void GLMMat4Str(__OUT__ char *const out, GLMMat4Ref a)
{
    sprintf_s(out, 512, "GLMMat2(%f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f, %f)", a[0], a[1], a[2], a[3], a[4], a[5], a[6], a[7], a[8], a[9], a[10], a[11], a[12], a[13], a[14], a[15]);
}

/**
* Computes Frobenius norm of a GLMMat4
*
* @param a reference to the matrix to calculate Frobenius norm of
* @returns Frobenius norm
*/
GLMRational GLMMat4Frob(GLMMat4Ref a)
{
    return (GLM_SQRT(GLM_POW(a[0], GLM_RATIONAL(2.0)) + GLM_POW(a[1], GLM_RATIONAL(2.0)) + GLM_POW(a[2], GLM_RATIONAL(2.0)) + GLM_POW(a[3], GLM_RATIONAL(2.0)) + GLM_POW(a[4], GLM_RATIONAL(2.0)) + GLM_POW(a[5], GLM_RATIONAL(2.0)) + GLM_POW(a[6], GLM_RATIONAL(2.0)) + GLM_POW(a[7], GLM_RATIONAL(2.0)) + GLM_POW(a[8], GLM_RATIONAL(2.0)) + GLM_POW(a[9], GLM_RATIONAL(2.0)) + GLM_POW(a[10], GLM_RATIONAL(2.0)) + GLM_POW(a[11], GLM_RATIONAL(2.0)) + GLM_POW(a[12], GLM_RATIONAL(2.0)) + GLM_POW(a[13], GLM_RATIONAL(2.0)) + GLM_POW(a[14], GLM_RATIONAL(2.0)) + GLM_POW(a[15], GLM_RATIONAL(2.0))));
}

/**
* Adds two GLMMat4's
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat4Add(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMMat4Ref b)
{
    out[0]  = a[0]  + b[0];
    out[1]  = a[1]  + b[1];
    out[2]  = a[2]  + b[2];
    out[3]  = a[3]  + b[3];
    out[4]  = a[4]  + b[4];
    out[5]  = a[5]  + b[5];
    out[6]  = a[6]  + b[6];
    out[7]  = a[7]  + b[7];
    out[8]  = a[8]  + b[8];
    out[9]  = a[9]  + b[9];
    out[10] = a[10] + b[10];
    out[11] = a[11] + b[11];
    out[12] = a[12] + b[12];
    out[13] = a[13] + b[13];
    out[14] = a[14] + b[14];
    out[15] = a[15] + b[15];
}

/**
* Subtracts matrix b from matrix a
*
* @param out reference to the receiving matrix
* @param a reference to the first operand
* @param b reference to the second operand
*/
void GLMMat4Subtract(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMMat4Ref b)
{
    out[0]  = a[0]  - b[0];
    out[1]  = a[1]  - b[1];
    out[2]  = a[2]  - b[2];
    out[3]  = a[3]  - b[3];
    out[4]  = a[4]  - b[4];
    out[5]  = a[5]  - b[5];
    out[6]  = a[6]  - b[6];
    out[7]  = a[7]  - b[7];
    out[8]  = a[8]  - b[8];
    out[9]  = a[9]  - b[9];
    out[10] = a[10] - b[10];
    out[11] = a[11] - b[11];
    out[12] = a[12] - b[12];
    out[13] = a[13] - b[13];
    out[14] = a[14] - b[14];
    out[15] = a[15] - b[15];
}


/**
* Multiply each element of the matrix by a scalar.
*
* @param out reference to the receiving matrix
* @param a reference to the matrix to scale
* @param b reference to amount to scale the matrix's elements by
*/
void GLMMat4MultiplyScalar(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMRational b)
{
    out[0]  = a[0]  * b;
    out[1]  = a[1]  * b;
    out[2]  = a[2]  * b;
    out[3]  = a[3]  * b;
    out[4]  = a[4]  * b;
    out[5]  = a[5]  * b;
    out[6]  = a[6]  * b;
    out[7]  = a[7]  * b;
    out[8]  = a[8]  * b;
    out[9]  = a[9]  * b;
    out[10] = a[10] * b;
    out[11] = a[11] * b;
    out[12] = a[12] * b;
    out[13] = a[13] * b;
    out[14] = a[14] * b;
    out[15] = a[15] * b;
}

/**
* Adds two GLMMat4's after multiplying each element of the second operand by a scalar value.
*
* @param out reference to the receiving vector
* @param a reference to the first operand
* @param b reference to the second operand
* @param scale the amount to scale b's elements by before adding
*/
void GLMMat4MultiplyScalarAndAdd(__OUT__ GLMMat4Ref out, GLMMat4Ref a, GLMMat4Ref b, GLMRational scale)
{
    out[0]  = a[0]  + (b[0]  * scale);
    out[1]  = a[1]  + (b[1]  * scale);
    out[2]  = a[2]  + (b[2]  * scale);
    out[3]  = a[3]  + (b[3]  * scale);
    out[4]  = a[4]  + (b[4]  * scale);
    out[5]  = a[5]  + (b[5]  * scale);
    out[6]  = a[6]  + (b[6]  * scale);
    out[7]  = a[7]  + (b[7]  * scale);
    out[8]  = a[8]  + (b[8]  * scale);
    out[9]  = a[9]  + (b[9]  * scale);
    out[10] = a[10] + (b[10] * scale);
    out[11] = a[11] + (b[11] * scale);
    out[12] = a[12] + (b[12] * scale);
    out[13] = a[13] + (b[13] * scale);
    out[14] = a[14] + (b[14] * scale);
    out[15] = a[15] + (b[15] * scale);
}

/**
* Returns whether or not the matrices have exactly the same values
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat4EqualsExact(GLMMat4Ref a, GLMMat4Ref b)
{
    return  a[0]  == b[0]  && a[1]  == b[1]  && a[2]  == b[2]  && a[3]  == b[3] &&
            a[4]  == b[4]  && a[5]  == b[5]  && a[6]  == b[6]  && a[7]  == b[7] &&
            a[8]  == b[8]  && a[9]  == b[9]  && a[10] == b[10] && a[11] == b[11] &&
            a[12] == b[12] && a[13] == b[13] && a[14] == b[14] && a[15] == b[15];
}


/**
* Returns whether or not the matrices have approximately the same elements in the same position.
*
* @param a reference to the first matrix.
* @param b reference to the second matrix.
* @returns true if the matrices are equal, false otherwise.
*/
bool GLMMat4Equals(GLMMat4Ref a, GLMMat4Ref b)
{
    GLMRational a0  = a[0],  a1  = a[1],   a2 = a[2],   a3 = a[3],
                a4  = a[4],  a5  = a[5],   a6 = a[6],   a7 = a[7],
                a8  = a[8],  a9  = a[9],  a10 = a[10], a11 = a[11],
                a12 = a[12], a13 = a[13], a14 = a[14], a15 = a[15];

    GLMRational b0  = b[0],  b1  = b[1],  b2  = b[2],   b3 = b[3],
                b4  = b[4],  b5  = b[5],  b6  = b[6],   b7 = b[7],
                b8  = b[8],  b9  = b[9],  b10 = b[10], b11 = b[11],
                b12 = b[12], b13 = b[13], b14 = b[14], b15 = b[15];

    return (GLM_ABS(a0  - b0)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a0),  GLM_ABS(b0)) ) &&
            GLM_ABS(a1  - b1)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a1),  GLM_ABS(b1)) ) &&
            GLM_ABS(a2  - b2)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a2),  GLM_ABS(b2)) ) &&
            GLM_ABS(a3  - b3)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a3),  GLM_ABS(b3)) ) &&
            GLM_ABS(a4  - b4)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a4),  GLM_ABS(b4)) ) &&
            GLM_ABS(a5  - b5)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a5),  GLM_ABS(b5)) ) &&
            GLM_ABS(a6  - b6)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a6),  GLM_ABS(b6)) ) &&
            GLM_ABS(a7  - b7)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a7),  GLM_ABS(b7)) ) &&
            GLM_ABS(a8  - b8)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a8),  GLM_ABS(b8)) ) &&
            GLM_ABS(a9  - b9)  <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a9),  GLM_ABS(b9)) ) &&
            GLM_ABS(a10 - b10) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a10), GLM_ABS(b10)) ) &&
            GLM_ABS(a11 - b11) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a11), GLM_ABS(b11)) ) &&
            GLM_ABS(a12 - b12) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a12), GLM_ABS(b12)) ) &&
            GLM_ABS(a13 - b13) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a13), GLM_ABS(b13)) ) &&
            GLM_ABS(a14 - b14) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a14), GLM_ABS(b14)) ) &&
            GLM_ABS(a15 - b15) <= GLM_EPSILON * GLM_MAX( GLM_RATIONAL(1.0), GLM_MAX(GLM_ABS(a15), GLM_ABS(b15)) ));
}

#endif