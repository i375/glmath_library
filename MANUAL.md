#GL MATH Programmer manual#

##Installation##

Clone glmath in your project folder:

```
cd your_project_folder

git clone https://bitbucket.org/9221145/glmath.git glmath
```

Include glmath.h in your c/c++ file:

```
#include "glmmath/glmath.h"
```

Thats all you need to do, library is header only and depends only on C Standard runtime library.

##Core information##

All library functions, strucutres, typedefs, definitions are prefixed with GLM(ex.: ```GLMVec2```, ```GLMVec2Add(result,a,b,)```...) 

Library supports two precision modes, ```float``` and ```double```. Default is ```float```. To switch to ```double``` precision(mind the performance cost), change ```#define GLM_PRECISION GLM_PRECISION_FLOAT```
in ```common.h``` to ```#define GLM_PRECISION GLM_PRECISION_DOUBLE```.

##Examples##

###Basic example###


```c++

#include "glmath/glmath.h"

int maint(void)
{
    // Allocating GLMVec2 on stack, and initializing with literals
    GLMVec2 v1 = { GLM_RATIONAL(1.0), GLM_RATIONAL(2.0) };

    // Allocating GLMVec2 on heap, heap allocated structures are automatically 
    // initialized with defaults values in case of GLMVec2 it's { 0.0,0.0 }
    GLMVec2Ref v2 = GLMVec2Create(); 

    // Setting vetor values with designated function
    GLMVec2Set(v2, GLM_RATIONAL(4.0), GLM_RATIONAL(3.0));

    //Adding v1 to v2 and writing results in v1
    GLMVec2Add(v1, v1, v2);

    //Freeing memory, allocated for v2
    GLM_FREE(v2);

    return 0;	
}

```

```GLM_RATIONAL(x)``` macro is for forcing you to write fractional value literals, to avoid convertion from integer types 
to fracionals and it also handles ```float``` vs ```double``` switch defined in ```common.h``` which controls the precision
of computations.
